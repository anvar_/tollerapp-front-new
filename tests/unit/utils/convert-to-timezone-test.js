import convertToTimezone from 'tollerapp-front/utils/convert-to-timezone';
import { module, test } from 'qunit';

module('Unit | Utility | convert to timezone');

// Replace this with your real tests.
test('it works', function(assert) {
  let result = convertToTimezone();
  assert.ok(result);
});
