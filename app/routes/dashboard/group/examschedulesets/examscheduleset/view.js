import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {
    return Ember.RSVP.hash({
    examscheduleset: this.modelFor('dashboard.group.examschedulesets.examscheduleset'),
    audios: this.store.query('audio',{filter: {user:this.get('session.data.user_id')}}),
    superaudios: this.store.query('audio' ,{filter: {user:1}})
  });
  },

setupController: function(controller ,model) {
  controller.set('examscheduleset',model.examscheduleset );
  controller.set('audios',model.audios );
  var audios = controller.get('audios');
  audios.get('content').pushObjects(model.superaudios.get('content'));
},

actions:{
  reloadModel:function(){
    this.refresh();
  }
}

});
