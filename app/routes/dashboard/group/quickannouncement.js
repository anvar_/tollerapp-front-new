import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service('session'),


  model: function() {
return Ember.RSVP.hash({
  user: this.store.findRecord('user', this.get('session.data.user_id')),
  quicknotifications: this.store.query('quicknotification' ,{filter: {user:this.get('session.data.user_id')}}),
});
},

setupController: function(controller ,model) {
controller.set('user',model.user);
controller.set('quicknotifications',model.quicknotifications);
},

actions: {
	    reloadModel: function() {
	      this.refresh();

	    }
	  }

});
