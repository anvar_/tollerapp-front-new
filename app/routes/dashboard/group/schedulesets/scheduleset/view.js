import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {
    return Ember.RSVP.hash({
    scheduleset: this.modelFor('dashboard.group.schedulesets.scheduleset'),
    audios: this.store.query('audio' ,{filter: {user:this.get('session.data.user_id')}}),
    superaudios: this.store.query('audio' ,{filter: {user:1}})
  });
  },

setupController: function(controller ,model) {
  controller.set('scheduleset',model.scheduleset );
  controller.set('audios',model.audios );
  var audios = controller.get('audios');
  audios.get('content').pushObjects(model.superaudios.get('content'));




}

});
