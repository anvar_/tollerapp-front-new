import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {
    return Ember.RSVP.hash({
    scheduleset: this.modelFor('dashboard.super.schedulesets.scheduleset'),
    audios: this.store.query('audio' ,{filter: {user:this.get('session.data.user_id')}}),
    
  });
  },

setupController: function(controller ,model) {
  controller.set('scheduleset',model.scheduleset );
  controller.set('audios',model.audios );
  // controller.set('assignations',model.assignations );



}

});
