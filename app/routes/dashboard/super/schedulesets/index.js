import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service('session'),

model:function(){
  return Ember.RSVP.hash({

    schedulesets:this.store.query('scheduleset', {filter: {user:this.get('session.data.user_id')}}),
    examschedulesets:this.store.query('examscheduleset', {filter: {user:this.get('session.data.user_id')}})



  });
},



setupController: function(controller ,model) {
  controller.set('schedulesets',model.schedulesets);
  controller.set('examschedulesets',model.examschedulesets);
}

});
