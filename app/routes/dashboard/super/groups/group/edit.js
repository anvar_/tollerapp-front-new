import Ember from 'ember';

export default Ember.Route.extend({

  model: function() {
    return Ember.RSVP.hash({
      group: this.modelFor('dashboard.super.groups.group'),
    });
  },

  setupController: function(controller ,model) {
    controller.set('group',model.group );
  }

});
