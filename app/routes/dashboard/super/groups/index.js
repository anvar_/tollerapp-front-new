import Ember from 'ember';

export default Ember.Route.extend({

model:function(){
  return Ember.RSVP.hash({
    groups:this.store.findAll('group',{reload:true})
  });
},



setupController: function(controller ,model) {
  controller.set('groups',model.groups);
}

});
