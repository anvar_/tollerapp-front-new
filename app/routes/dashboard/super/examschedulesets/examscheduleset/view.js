import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {
    return Ember.RSVP.hash({
    examscheduleset: this.modelFor('dashboard.super.examschedulesets.examscheduleset'),
    audios: this.store.query('audio',{filter: {user:this.get('session.data.user_id')}}),
    // examassignations:this.store.findAll('examassignation',{reload:true})
  });
  },

setupController: function(controller ,model) {
  controller.set('examscheduleset',model.examscheduleset );
  controller.set('audios',model.audios );
  // controller.set('examassignations',model.examassignations );
},

actions:{
  reloadModel:function(){
    this.refresh();
  }
}

});
