import Ember from 'ember';

export default Ember.Route.extend({

  fcmajax: Ember.inject.service(),
  session: Ember.inject.service('session'),




  beforeModel: function() {

   if(!Ember.isEqual('local', this.get('session.data.role'))){
     this.transitionTo('dashboard');
   }
  },


  setupController: function(controller ,model) {
    // this.pingNow(controller);
    // this.pingTwentySeconds(controller);
    // this.checkTenSeconds(controller);




  },


    pingNow:function(controller){


      console.log('pinging now....')
      var self=this;

      // *******************pining device


    console.log("SENDING MESSAGE TO DEVICE ........");
      self.get('fcmajax').request('https://fcm.googleapis.com/fcm/send', {
       method: 'POST',
       contentType: 'application/json; charset=utf-8',
      headers:{
        'Authorization':'key=AIzaSyBDg_tQOP4dj4V0o6ujPboARZHfOTa40wM'
      },

      data:JSON.stringify({
      "registration_ids":["fnMXyZ92Jvs:APA91bEu6JAM3ec8TdOYNHmJ0z2uN1qqpg2crVtkUyoHj9fo74e19mYFM1u55PsJzM7sKVKvpUpR_Vx44b1806MkITgA8nO35CNg4cqIrymOzp1LP-pBbYJDNNDiLB8q2cjk-Mm_W_xE"],
      "notification": {
          "title":"Title of your notification",
          "body":"content of your notification"
      },
      "data": {
        "amplifier" : "unchanged", //turnon/turnoff/unchanged
        "mute" : "unchanged",   //mute/unmute/unchanged
        "sync" : "sync",   //sync/unchanged
      }
    })

     }).then(function(json){

       console.log(json);
       var setPending = self.setPending(controller,'pendingonlinestatus');
       setPending.then(function(fcm){


       },function(error){

       });

     });


    },




    checkTenSeconds:function(controller){
      self = this;
      var homecontroller = self.controllerFor('dashboard.local.home');


      console.log("SENDING MESSAGE TO DEVICE ........");


      self.controller.set('deviceOnlinestatus','TURNED ON');
        self.get('fcmajax').request('https://fcm.googleapis.com/fcm/send', {
         method: 'POST',
         contentType: 'application/json; charset=utf-8',
        headers:{
          'Authorization':'key=AIzaSyBDg_tQOP4dj4V0o6ujPboARZHfOTa40wM'
        },

        data:JSON.stringify({
        "registration_ids":["e0RevdlfapI:APA91bF5bqH4GDlqjhcEUfrBIWLiTeaAS_7IoXHOVwHbJrSFywxg-sb2wJ9hjC-FJ8Wok7Qi3msEHa228b1GUw6ootR0SdyqZ7Ig3t9FBSI5K6VGSvPgJFHgyCZVWK9A_rPEubj5Velq"],
        "data":{"message":"SYNC"},

      })

    }).then(function(json,controller){

         console.log(json);
        //  console.log("calling set pening..");
         var setPending = self.setPending(controller,'pendingonlinestatus');
         setPending.then(function(fcm){

                     Em.run.later(function(){
               console.log('Checing Ten Seconds......');

                       self.store.findRecord('fcm',self.get('session.data.fcmid')).then(function(fcm){

                         console.log('Pending Online Status: ',fcm.get('pendingonlinestatus'));
                         console.log('Device Audio Status : ',fcm.get('deviceaudiostatus'));
                         console.log('Device Mute Status : ',fcm.get('devicemutestatus'));

                         if(fcm.get('pendingonlinestatus') == 'updated'){
                            homecontroller.set('deviceOnlinestatus','ONLINE');
                            homecontroller.set('deviceAmplifierstatus',fcm.get('deviceaudiostatus'));
                            homecontroller.set('deviceMutestatus',fcm.get('devicemutestatus'));
                         }
                         else{
                           homecontroller.set('deviceOnlinestatus','OFFLINE');
                           homecontroller.set('deviceAmplifierstatus','CANTCONNECT');
                           homecontroller.set('deviceMutestatus','CANTCONNECT');
                         }
                        //  homecontroller.set('deviceOnlinestatus',fcm.get('pendingonlinestatus'));
                        //  homecontroller.set('deviceAmplifierstatus',fcm.get('pendingonlinestatus'));
                        //  homecontroller.set('deviceOnlinestatus',fcm.get('pendingonlinestatus'));
                         self.checkTenSeconds(controller);

                       })
                     }, 10000);

         },function(error){

console.log("PINGING DEVICE FAILDED......");
         });

       });


    },




    setPending:function(controller,setwhat){
      console.log("called set pending..");



      var self=this;


      var request = new Ember.RSVP.Promise(function(resolve, reject) {

      self.store.findRecord('fcm',self.get('session.data.fcmid')).then(function(fcm){

        fcm.set('pendingonlinestatus','pending');
        fcm.save().then(function(){
          console.log(setwhat,"Set to Pending...")
        resolve(fcm);
      }).catch(function(){
        reject("something went wrong");
      });

      })
    });
    return request;

    },








    updateStatus:function(controller,fcm){

      controller.set('deviceOnlinestatus',fcm.get('deviceonlinestatus'));
      controller.set('deviceAudiostatus',fcm.get('deviceaudiostatus'));
      controller.set('deviceMutestatus',fcm.get('deviceaudiostatus'));

    },




    actions:{
      ampOn:function(){
        console.log("amplifier is turned on");
      }
    }



});
