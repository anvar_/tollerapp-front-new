import Ember from 'ember';

export default Ember.Route.extend({

  model:function(){

        return Ember.RSVP.hash({
   versions: this.store.findAll('version' ,{reload :true})
   });

 },

  setupController: function(controller ,model) {
    controller.set('versions',model.versions);
  }
});
