import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
model:function(){
  return Ember.RSVP.hash({
    tickets: this.store.query('ticket' ,{filter: {user:this.get('session.data.user_id')}}),
  });
},



setupController: function(controller ,model) {
  controller.set('tickets',model.tickets);
}

});
