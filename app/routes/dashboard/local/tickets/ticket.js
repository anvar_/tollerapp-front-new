import Ember from 'ember';

export default Ember.Route.extend({
  model: function(params) {
return Ember.RSVP.hash({
  ticket: this.store.findRecord('ticket', params.id ),
  attachments: this.store.query('attachment',{filter: {ticket:params.id}})
});
},

setupController: function(controller ,model) {

   controller.set('ticket',model.ticket );
   controller.set('attachments',model.attachments );

},

actions:{
  reloadModel:function(){
    this.refresh();
  }
}


});
