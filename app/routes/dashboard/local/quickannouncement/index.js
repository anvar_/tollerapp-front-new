import Ember from 'ember';

export default Ember.Route.extend({
    session: Ember.inject.service('session'),
    model:function(){
        return Ember.RSVP.hash({
          quickannouncements: this.store.query('quicknotification' ,{filter: {user:this.get('session.data.user_id')}}),
        
        });
      },
      
      
      
      setupController: function(controller ,model) {

        controller.set('quickannouncements',model.quickannouncements);
      }

    

});
