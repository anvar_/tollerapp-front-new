import Ember from 'ember';

export default Ember.Route.extend({

    model: function(params) {
        return Ember.RSVP.hash({
          quickannouncement: this.store.findRecord('quicknotification', params.id ),
        });
        },

        setupController:function(controller,model){
            controller.set('quickannouncement', model.quickannouncement);
        }
});
