import Ember from 'ember';

export default Ember.Route.extend({
  session: Ember.inject.service('session'),


  model: function() {
return Ember.RSVP.hash({
  user: this.store.findRecord('user', this.get('session.data.user_id') ),
  audios: this.store.query('audio' ,{filter: {user:this.get('session.data.user_id')}}),
  superaudios: this.store.query('audio' ,{filter: {user:1}}),


});
},

setupController: function(controller ,model) {
controller.set('user',model.user);
controller.set('audios',model.audios);
controller.set('superaudios',model.superaudios);

},

actions: {
	    reloadModel: function() {

	      this.refresh();

	    }
	  }

});
