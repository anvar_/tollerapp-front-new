import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {

    var self = this;

    return self.store.findRecord('user',self.get('session.data.user_id') ).then(function(user){
      return Ember.RSVP.hash({
      scheduleset: self.modelFor('dashboard.local.schedulesets.scheduleset'),
      audios: self.store.query('audio' ,{filter: {user:self.get('session.data.user_id')}}),
      // assignations:self.store.findAll('assignation',{reload:true}),
      userschedulesets: self.store.query('scheduleset' ,{filter: {user:self.get('session.data.user_id')}}),
      superaudios: self.store.query('audio' ,{filter: {user:1}}),
      groupaudios: self.store.query('audio' ,{filter: {user:user.get('group.id')}}),
      superuserscheduleset: self.store.findRecord('scheduleset' ,1),
    });
    });








  },

setupController: function(controller ,model) {
  controller.set('scheduleset',model.scheduleset );
  controller.set('userschedulesets',model.userschedulesets );
  controller.set('audios',model.audios );
  // controller.set('assignations',model.assignations );
  controller.set('superuserscheduleset',model.superuserscheduleset );

  var audios = controller.get('audios');
  audios.get('content').pushObjects(model.superaudios.get('content'));
  audios.get('content').pushObjects(model.groupaudios.get('content'));







}

});
