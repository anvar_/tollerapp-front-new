import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {
    return Ember.RSVP.hash({
    payment: this.modelFor('dashboard.local.payments.payment'),
  });
  },

setupController: function(controller ,model) {
  controller.set('payment',model.payment );
  
}

});
