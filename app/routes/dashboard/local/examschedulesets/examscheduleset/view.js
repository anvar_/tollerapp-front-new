import Ember from 'ember';

export default Ember.Route.extend({
session: Ember.inject.service('session'),
  model: function() {


    var self = this;


    return self.store.findRecord('user',self.get('session.data.user_id') ).then(function(user){



    return Ember.RSVP.hash({
    examscheduleset: self.modelFor('dashboard.local.examschedulesets.examscheduleset'),
    audios: self.store.query('audio',{filter: {user:self.get('session.data.user_id')}}),
    userexamschedulesets: self.store.query('examscheduleset' ,{filter: {user:self.get('session.data.user_id')}}),
    superaudios: self.store.query('audio' ,{filter: {user:1}}),
    groupaudios: self.store.query('audio' ,{filter: {user:user.get('group.id')}}),
  });


});

  },

setupController: function(controller ,model) {
  controller.set('examscheduleset',model.examscheduleset );
  controller.set('audios',model.audios );
  var audios = controller.get('audios');
  audios.get('content').pushObjects(model.superaudios.get('content'));
  audios.get('content').pushObjects(model.groupaudios.get('content'));
  controller.set('userexamschedulesets',model.userexamschedulesets );
},

actions:{
  reloadModel:function(){
    this.refresh();
  }
}

});
