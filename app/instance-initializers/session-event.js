export function initialize(instance) {
  const applicationRoute = instance.container.lookup('route:application');
  const session          = instance.container.lookup('service:session');

  // In this function we are copying and pasting data.authenticated.user_id to data.user_id , and use it inside the application
  // to identify current user
  session.on('authenticationSucceeded', function() {
    // applicationRoute.transitionTo('index');
    session.set('data.user_id', session.get('data.authenticated.user_id'));
    session.set('data.email', session.get('data.authenticated.email'));
    session.set('data.role', session.get('data.authenticated.role'));
    session.set('data.timezone', session.get('data.authenticated.timezone'));
    session.set('data.fcmid', session.get('data.authenticated.fcmid'));

    // console.log("authtntication suceed");

    // console.log(session.get('data'));
    // this.get('session').set('data.user_id', user.id);
  });
  session.on('invalidationSucceeded', function() {
    session.set('data.user_id', '');
    session.set('data.email', '');
    session.set('data.role', '');
    session.set('data.timezone', '');
    session.set('data.fcmid', '');

    session.set('data.impersonated',false);
    applicationRoute.transitionTo('login');
    // console.log("invalidationSucceeded");
  });
}

export default {
  initialize,
  name:  'session-events',
  after: 'ember-simple-auth'
};
