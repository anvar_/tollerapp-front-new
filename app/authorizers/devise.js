import DeviseAuthorizer from 'ember-simple-auth/authorizers/devise';

export default DeviseAuthorizer.extend({
  identificationAttributeName: 'username',
  actions: {
    authorize: function(){
      var self = this;
      this.get('session').authorize('authorizer:devise', () => {
        console.log(self.get('session'));
      });
    }
  }
});
