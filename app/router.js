/*
Always comment code, bcoz ,
when u code , you and god knows what it is.
six months later only god knows
*/



import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});


// Loading Animations  --- Promise try catch ---- Button Disabled for false input --  Disable Buttons until promise resolve

Router.map(function() {
  this.route('login'); //D-D-D-D
  this.route('dashboard', {path: '/'}, function() {

    //START OF LOCAL USERS //
    this.route('local', function() {
      this.route('mediamanager'); // D-D-D-D
      this.route('schedulesets', function() {
        //D-D-D-D
        this.route('scheduleset',{path: ':id'} , function() {
          this.route('view');//D-D-D-D
        });
        this.route('regular');
        this.route('exam');
      });


      this.route('tickets', function() {
        this.route('ticket', {path: ':id'});
        this.route('new');
      });

      // this.route('tickets', function() {
      //   this.route('ticket', {path: ':id'} ,  function() {
      //     this.route('view');
      //     this.route('edit');
      //   });
      //   this.route('new');
      // });
      this.route('configuration');
      this.route('profile');
      this.route('examschedulesets', function() {//D-D-D-D
        this.route('examscheduleset', {path: ':id'} , function() {
          this.route('view'); //D-D-D-D
        });
      });
      this.route('payments', function() {  ////////////////////////////SKIPPED  FOR REENGINEERING
        this.route('payment',  {path: ':id'} , function() {
          this.route('view');
        });
        this.route('new');
      });
      this.route('log');

      this.route('quickannouncement', function() {
        this.route('quickannouncement', {path: ':id'});
        this.route('new');
        this.route('scheduleannouncement');
      });
      this.route('home');
      this.route('fire-evacuation');
      this.route('t-radio');
      this.route('stats');
    });
    //END OF LOCAL USERS //


    //START OF GROUP USERS //
    this.route('group', function() {
      this.route('users', function() { //D-D-D-D
        this.route('user' , {path: ':id'} , function() { //D-D-D-D
          this.route('view');//D-D-D-D
          this.route('edit');
        });
      });
      this.route('schedulesets', function() {///////////////////////// SKIPPED TO FIX Promise
        this.route('scheduleset',  {path: ':id'} , function() {
          this.route('view');
        });
      });
      this.route('examschedulesets', function() { ///////////////////////// SKIPPED TO FIX Promise
        this.route('examscheduleset',  {path: ':id'} , function() {
          this.route('view');
        });
      });
      this.route('mediamanager'); //D-D-D-D
      this.route('quickannouncement');
    });
    //END OF GROUP USERS //

    //START OF SUPER USER //

    this.route('super', function() {
      this.route('users', function() { //D-D-D-D
        this.route('user',  {path: ':id'} , function() { //D-D-D-D
          this.route('view'); //D-D-D-D
          this.route('edit'); //D-D-D-D
        });
        this.route('new'); //D-D-D-D
      });
      this.route('tickets', function() {///////////////////////// SKIPPED TO REENGINEERING
        this.route('ticket', {path: ':id'} , function() {
          this.route('view');
          this.route('edit');
        });
      });
      this.route('settings', function() {
        this.route('wallet');  ////////////////SKIPPED FOR REENGINEERING
        this.route('quotes', function() {
          this.route('quote'); //D-D-D-D
          this.route('backgroundimage'); //D-D-D-D
        });
        this.route('calendar');
        this.route('profile');
      });
      this.route('schedulesets', function() {
        this.route('scheduleset',  {path: ':id'} ,  function() {
          this.route('view');
        });
      });
      this.route('examschedulesets', function() {
        this.route('examscheduleset',  {path: ':id'} ,  function() {
          this.route('view');
        });
      });
      this.route('groups', function() {
        this.route('group' ,  {path: ':id'} , function() {
          this.route('view'); //D-D-D-D
          this.route('edit'); //D-D-D-D
        });
        this.route('new'); //D-D-D-D
      });
      this.route('mediamanager');
    });

    //END OF SUPER USER //
  });

  this.route('dashbord', function() {
    this.route('local', function() {});
  });
});

export default Router;



 /*
 Tickets Module:
 =============
* Should have a support role.  //postponed
* Tickets should have a conversation type mechanism
* Attachments needed.
* Ticket count should be on the dashboard of super admin
*

Scheduleset Module:
==================
* Scheduleset edit description is missing.                        // Lre-Sre-Gre
* Scheduleset color notifications as mentioned.:assigned-tick,timings-yellow  //Lre-Sre-Gre
* Timings should not repeat.                                      //Lre-Sre-Gre
* Delete all timings button.                                      //Lre-Sre-Gre
* Ask popup before reassinging.                                   //Lre-Sre-Gre
* Remove assignation automatically if all timings are deleted.    //Lre-Sre-Gre
* Disable assignation button if no timings are present            //Lre-Sre-Gre
* Start default time at 8 am                                      //Lre-Sre-Gre
* Add 40 mins to each new timings.                                //Lre-Sre-Gre
* Add zero to hours and minutes                                   //Lre-Sre-Gre
* "Save and add another button" needed for timings.               //Lre-Sre-Gre
* Change copy text to import,                                     //Lre-Sre-Gre
* default mp3 file for timings.                                   //Lre-Sre-Gre
* Dont save timing if audio is not selected.                      //Lre-Sre-Gre
* Copy audio also with copy Scheduleset.                          //Lre-Sre-Gre


New Audio Logic:
======================
* Keep 100 files as default for super admin.
* Local admins could add 20 files
* so path should be dynamic.

payment Module:
===============
* Add Payment from super
* Remove wallet payment.
* Show only history.
* Provide cash/cheque methods as payment methods.


Log Module:
===============
* deactivate the link.


Impersonate:
=============
* Redirect back to useres page after deimpersonate //Not Possible without node js


Pending Changes:
===============
* promise catch for broadcast
* do payment Module
*



1.) When schedules are edited it moves to last in the list.     //cant reproduce
2.)In fact every schedule set has its own location                // cant reproduce
3.) Back option from exam schedule setting lands you to regular schedule.    //its tab
4.) Change of name to Toller Pro above menu buttons.                        ------------
5.) Super user login User profile save button doesn't appears to work,  but it's getting saved     //fixed
6.) creating new user doesn't seem to work.                        /fixed
7.) Editing group details not found            //fixed
8.) Update the title of dummy schedule menu to 'Dummy schedules'                         ---------
9.) Admin doesn't require to edit the tickets created by the user.
10.) Upload media doesn't work                        //cant reproducce
11.) Schedule set heading. It's 'Schedule sets'   -------------


//As Discussedd on 18/06

* Local admin mediamanager will list two folders , local and super.
* Local admin could upload mp3 files to his folder , also he could drag and drop default files from super admin.
* Which could be fetched in to the list drop down.
*



Instructions to fetch data from api.

*give usename and password.  and get the token.
* attach token header to get data.
* Example scenario:
        * When a change is made server will ping the device and device will initiate a request for all schedulesets
          their timings and mp3 file urls.
        * In that scenario, all schedulesets with that particular user id is fetched and looped to get  all other data.




* user delete
* group Delete , already assigned user should come to unassigned group
* group edit
* super user should be able to add payment to each user.
*

*Phase 1 Pending changes consolidated:
=====================================
* Group Broadcast-----------------------------------------------------------Pending
* Local Admin mediamanager could  super media to his folder-----------------Cant Be Fixed
* Super admin mediamanager--------------------------------------------------Done
* Quick Notification for local and group admin------------------------------Done
* User Delete option -------------------------------------------------------Done
* language support Needed --------------------------------------------------Done
* Language Support should be saved while creating and editing user.---------Done
* user unique id should be created while saving.----------------------------Done
* Group Delete option , if deleted assigned user should  unassigned .-------Done
* Group edit name.----------------------------------------------------------Done
* Super user add payment.---------------------------------------------------Done
* Add new Timings should save previous timings.-----------------------------Done
* Unassign all button needed in timings.------------------------------------Done

* Things to Do after Dashboard Design:
* Dashboard clock should by sync with server timezone.----------------------Done
* Show Scheduule timings in elastic bar.------------------------------------Pending
* Show next audio.----------------------------------------------------------Pending
* Button to turn on amplifier.----------------------------------------------Pending
* Count down next timing interval.------------------------------------------Pending


*optimize payment
* user list sotrt
*



Tables accessed by api device:
=============================
1. user    --
2. schedulesets---
3. timings
4. assignations
5. Examscheduleset---
6. Examtimings
7. examassignations.
8. audio
9. quicknotifications

















  */
