import Ember from 'ember';
import AjaxService from 'ember-ajax/services/ajax';
import ENV from '../config/environment';




export default AjaxService.extend({
  host: ENV.APP.host,

  session: Ember.inject.service(),
  contentType: 'application/json; charset=utf-8',

  headers: Ember.computed('session.data.authenticated', {
    get() {
      let headers = {};
      // const authToken = this.get('session.data.authenticated.token');
      var authenticated = this.get('session.data.authenticated');

      if (authenticated) {
        headers['Authorization'] = 'Token token="'+ authenticated.token +'", username="'+ authenticated.username +'"';
      }
      return headers;
    }
  })


  // headers: Ember.computed({
  //   get() {
  //     let headers = {};
  //     // const authToken = this.get('session.data.authenticated.token');
  //
  //     // var key='AAAAsNIevl8:APA91bHoDECJr0sRtHipJBtFdxrBW_dlZlIv7FICWTlICVrcIHQ0oE7Fev3dsBPMj3nWZh0fOF7bVO2C3T3iBsrKS_6bgAZZC_N9yJJFd2ceG9WZtkHoH2iV-aR4EKUjEVmXz-wKSnbM';
  //     var key='aaaaaaaaaaaaaaaaaaaaaaaaaaa';
  //
  //     if (authenticated) {
  //       headers['Authorization'] = 'key='+key;
  //     }
  //     return headers;
  //   }
  // })
});
