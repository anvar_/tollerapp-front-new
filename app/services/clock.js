import Ember from 'ember';
import ClockService from 'ember-clock/services/clock';




export default ClockService.extend({
  session: Ember.inject.service('session'),
  converttotimezone: Ember.inject.service('converttotimezone'),
  moment: Ember.inject.service(),


   tConvert (time) {
     // Check correct time format and split into components
    time = time.toString ().match (/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice (1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    return time.join (''); // return adjusted time or original string
  },



  setTime() {

var self=this;
// var d = new Date();
// this.get('converttotimezone').converttotimezone();
var d = this.get('converttotimezone').converttotimezone();
var hh = d.getHours();
var m = d.getMinutes();
var s = d.getSeconds();
var dd = "AM";
var h = hh;
if (h >= 12) {
  h = hh - 12;
  dd = "PM";
}
if (h === 0) {
  h = 12;
}
m = m < 10 ? "0" + m : m;
s = s < 10 ? "0" + s : s;
// h = h < 10 ? "0" + h : h;




  self.setProperties({
    second: s,
    minute: m,
    hour:   h,
    ampm:dd
  });
},
});
