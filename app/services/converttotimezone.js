import Ember from 'ember';

export default Ember.Service.extend({
  session: Ember.inject.service('session'),
  moment: Ember.inject.service(),


//Function returns current time in user's timezone
  converttotimezone: function() {
    var timezone = this.get('session.data.timezone');
    if(!timezone){
      timezone = 'Asia/Kolkata';
    }

// timezone = 'Europe/Rome';

    var offset = moment().tz(timezone)._offset;//getting offset of user timezone in min
    let date = new Date();
    date.setTime( date.getTime() + offset*60*1000 );  //setting date to utc time
    var utc = date.getTime() + (date.getTimezoneOffset() * 60000); // adding offset of user timezone to get time there

    return  new Date(utc);
   }
});
