import Ember from 'ember';

export default Ember.Controller.extend({

paymentmethod:'cash', //REVIEW

actions:{

  selectWallet:function(){
    this.set('paymentmethod','wallet');
  },
  selectCash:function(){
    this.set('paymentmethod','cash');
  },
  payByWallet:function(user){
    var controller = this;

    let paymentamount = user.get('paymentamount');
    let previouswalletbalance = user.get('walletbalance');
    let afterwalletbalance = parseFloat(previouswalletbalance) - parseFloat(paymentamount);
    let today = new Date();

    if(previouswalletbalance < paymentamount){
      controller.notifications.addNotification({
        message:  'Not Enough balance in your Wallet' ,
        type: 'error',
        autoClear: true
      });
    }
    else{

    let newPayment = controller.store.createRecord('payment',{
      user:user,
      paymentdate:today,
      amount:paymentamount,
      previouswalletbalance:previouswalletbalance,
      afterwalletbalance:afterwalletbalance,
      paymentmethod:'wallet'
    });

//TODO: walletpayment is actually created for keep history of client funding super admin to replenish his wallet

    let newWalletPayment = controller.store.createRecord('walletpayment',{
      user:user,
      paymentdate:today,
      amount:paymentamount,
      previouswalletbalance:previouswalletbalance,
      afterwalletbalance:afterwalletbalance
    });

    user.set('walletbalance',afterwalletbalance);
    user.save();

    newPayment.save().then(function(){
    newWalletPayment.save();

    });
}// end of else

  },
  payByCash:function(user){

    var controller = this;

    let paymentamount = user.get('paymentamount');
    let previouswalletbalance = user.get('walletbalance');

    let today = new Date();


    let newPayment = controller.store.createRecord('payment',{
      user:user,
      paymentdate:today,
      amount:paymentamount,
      previouswalletbalance:previouswalletbalance,
      afterwalletbalance:previouswalletbalance,
      paymentmethod:'cash'
    });

    newPayment.save();

  },
}
});
