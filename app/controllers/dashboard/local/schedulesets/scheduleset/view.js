import Ember from 'ember';





export default Ember.Controller.extend({

  ajax: Ember.inject.service(),
  session: Ember.inject.service('session'),


//if timings are empty disabled asignation buttons
assignationDisabled:Ember.computed('schedulesetTimings',function(){
  var schedulesetTimingslength = this.get('schedulesetTimings.length');
  var schedulesetAssignations = this.get('scheduleset').get('assignations');
  if(schedulesetTimingslength===0){
    schedulesetAssignations.then(function(assignations){
      assignations.forEach(function(assignation){
        assignation.destroyRecord();
      });
    });
    return 'disabled';
  }else{
    return '';
  }
}),




  buttonDisabled:false,
  //To Get all schedulesets execept this one for copying option
  routeuserschedulesets:Ember.computed('userschedulesets','scheduleset',function(){
    var scheduleset = this.get('scheduleset');
    var userschedulesets = this.get('userschedulesets');

    return userschedulesets.filter(function(item){
      return item.get('id') !== scheduleset.get('id');
    });
  }),

  sortPropertiestimings: ['time:asc'],
  schedulesetTimings: Ember.computed.sort('scheduleset.timings', 'sortPropertiestimings'),

  // defines total count to prevent exceeding 25
  schedulesetTimingsCount: Ember.computed.alias('scheduleset.timings.length'),
  maxassignation:24,



  actions:{



unAssignall:function(scheduleset){
  var controller = this;
  var allassignations = scheduleset.get('assignations');
  allassignations.then(function(allassignations){

    var allassignationsprocessed = 0;

  var allassignationslength = scheduleset.get('assignations.length');
  allassignations.forEach(function(assignation){
    assignation.destroyRecord().then(function(){
      allassignationsprocessed++;
      if(allassignationsprocessed === allassignationslength){
        controller.notifications.addNotification({
            message:  'All assignations deleted !' ,
            type: 'success',
            autoClear: true
          });
      }
    }).catch(function(){
        controller.notifications.addNotification({
            message:  'Something Went Wrong !' ,
            type: 'error',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
    });
  });
  });

},

    deleteAllTimings:function(scheduleset){
      var controller = this;
      controller.set('buttonDisabled',true);

      var schedulesetTimings = scheduleset.get('timings');
      var schedulesetTimingsprocessed = 0;
      var schedulesetTimingslength = scheduleset.get('timings.length');

      schedulesetTimings.forEach(function(schedulesetTiming){
        schedulesetTiming.destroyRecord().then(function(){
          schedulesetTimingsprocessed++;
          if(schedulesetTimingsprocessed ===  schedulesetTimingslength){
            controller.notifications.addNotification({
              message:  'Timings deleted !' ,
              type: 'success',
              autoClear: true
            });
            controller.set('buttonDisabled',false);
          }

        }).catch(function(){
          controller.notifications.addNotification({
            message:  'Something Went Wrong !' ,
            type: 'error',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
        });

      });

    },



editDescription:function(scheduleset){
  scheduleset.set('editdescription',true);
},


saveDescription:function(scheduleset){
  var controller = this;

  controller.set("buttonDisabled",true);
  scheduleset.save().then(function(){
  scheduleset.set('editdescription',false);
  controller.set("buttonDisabled",false);
}).catch(function(){
  controller.notifications.addNotification({
    message:  'Something Went Wrong !' ,
    type: 'error',
    autoClear: true
  });
  scheduleset.set('editdescription',false);
  controller.set("buttonDisabled",false);
});

},


    sendToDevice:function(scheduleset){
      var controller = this;


scheduleset.get('timings').forEach(function(timing){
  var audio  = timing.get('audio');
  if(audio){
  audio.then(function(audio){
     return controller.get('ajax').request('http://192.168.0.102:8080/schedulesets', {
      method: 'POST',
      data: {
        scheduleset: JSON.stringify(scheduleset),
        audio:audio.get('fullurl')
      }
    });
  });
  }
});

    },



    //For copying Super User scheduleset
    copySuperScheduleset:function(){
      var controller = this;
      controller.notifications.addNotification({
        message:  'Copying.... Please Wait.' ,
        type: 'info',
      });
      controller.set('buttonDisabled',true);

      var scheduleset = controller.get('scheduleset'); //current scheduleset
      var superuserscheduleset = controller.store.findRecord('scheduleset',1);// this is static

superuserscheduleset.then(function(superuserscheduleset){

        var supertiminglength = superuserscheduleset.get('timings.length');
        var timings = superuserscheduleset.get('timings');

      //Superuser scheduleset should be always 1

      //Deleting all timings of current scheduleset
      if(supertiminglength === 0 ){
        controller.notifications.clearAll();
        controller.notifications.addNotification({
          message:  'Timings are empty' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);

    }else{



      var timingitemsProcessed = 0;
      var timinglength = scheduleset.get('timings.length');
      var supertimingitemsProcessed = 0;

if(timinglength === 0){





    timings.then(function(timings){

      timings.forEach(function(usertiming){
        var time = usertiming.get('time');
        var newTiming =   controller.store.createRecord('timing',{
          time:time,
          scheduleset:scheduleset,
          audio:1
        });

        newTiming.save().then(function(){
          supertimingitemsProcessed++;
          if(supertimingitemsProcessed === supertiminglength) {
            controller.notifications.clearAll();
            controller.notifications.addNotification({
              message:  'Scheduleset Copied' ,
              type: 'success',
              autoClear: true
            });
            controller.set('buttonDisabled',false);

          }
        });

      });

    });


}else{



      scheduleset.get('timings').forEach((timing) => {
        timing.destroyRecord().then(function(){
          timingitemsProcessed++;

          if(timingitemsProcessed === timinglength) {

              var timings = superuserscheduleset.get('timings');

              timings.then(function(timings){

                timings.forEach(function(usertiming){
                  var time = usertiming.get('time');
                  var audio = usertiming.get('audio');
                  var newTiming =   controller.store.createRecord('timing',{
                    time:time,
                    scheduleset:scheduleset,
                    audio:audio
                  });

                  newTiming.save().then(function(){
                    supertimingitemsProcessed++;
                    // console.log(supertimingitemsProcessed , supertiminglength);
                    if(supertimingitemsProcessed === supertiminglength) {
                      controller.notifications.clearAll();
                      controller.notifications.addNotification({
                        message:  'Scheduleset Copied' ,
                        type: 'success',
                        autoClear: true
                      });
                      controller.set('buttonDisabled',false);

                    }
                  });

                });

              });



          }
        });
      });



}
}
});

    },


    copyOtherScheduleset:function(){
      var controller = this;

      var selectedscheduleset = this.get('userscheduleset');//selected scheduleset
      var currentscheduleset = this.get('scheduleset'); //current scheduleset

      if(selectedscheduleset){

        controller.notifications.addNotification({
          message:  'Copying.... Please Wait.' ,
          type: 'info',
        });
        controller.set('buttonDisabled',true);

      var timingitemsProcessed = 0;
      var currentschedulesettimingslength = currentscheduleset.get('timings.length');
      var selectedschedulesettimingslength = selectedscheduleset.get('timings.length');
      var selectedtimings = selectedscheduleset.get('timings');


      if(selectedschedulesettimingslength===0){
        controller.notifications.clearAll();
        controller.notifications.addNotification({
          message:  'Timings are empty' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      }else{

  if(currentschedulesettimingslength ===0){
    var usertimingitemsProcessed = 0;


    selectedtimings.then(function(timings){
      timings.forEach(function(usertiming){
        var time = usertiming.get('time');
        var audio = usertiming.get('audio');
        var newTiming =   controller.store.createRecord('timing',{
          time:time,
          scheduleset:currentscheduleset,
          audio:audio
        });
        newTiming.save().then(function(){
          usertimingitemsProcessed++;
          if(usertimingitemsProcessed === selectedschedulesettimingslength){
            controller.notifications.clearAll();
            controller.notifications.addNotification({
              message:  'Scheduleset Copied' ,
              type: 'success',
              autoClear: true
            });
            controller.set('buttonDisabled',false);
          }
        });
      });
    });
  }else{ // if timings are more than 0
      currentscheduleset.get('timings').forEach(function(timing) {
        timing.destroyRecord().then(function(){
          timingitemsProcessed++;
          if(timingitemsProcessed === currentschedulesettimingslength ){
            var usertimingitemsProcessed = 0;


            selectedtimings.then(function(timings){
              timings.forEach(function(usertiming){
                var time = usertiming.get('time');
                var audio = usertiming.get('audio');
                var newTiming =   controller.store.createRecord('timing',{
                  time:time,
                  scheduleset:currentscheduleset,
                  audio:audio
                });
                newTiming.save().then(function(){
                  usertimingitemsProcessed++;
                  if(usertimingitemsProcessed === selectedschedulesettimingslength){
                    controller.notifications.clearAll();
                    controller.notifications.addNotification({
                      message:  'Scheduleset Copied' ,
                      type: 'success',
                      autoClear: true
                    });
                    controller.set('buttonDisabled',false);
                  }
                });
              });
            });
          }
        });
      });
    }
}


}else{
  controller.notifications.addNotification({
    message:  'Please select a scheduleset' ,
    type: 'warning',
    autoClear: true
  });
}
    },

    saveAssignation:function(day,scheduleset){

      // if assigned , check assignation is linked to only one scheduleset, if exist delete the assignation , and create new assignation with the scheduleset
      var controller = this;
      if(controller.get('schedulesetTimings.length')!==0){


      controller.set('buttonDisabled',true);

      controller.store.query('assignation', {filter: {day: day,user:controller.get('session.data.user_id')}})
      .then(function(prevAssignation) {

      var prevassign = prevAssignation.get("firstObject");


      if(prevassign){

          // delete any other assignation and save current
          //if prevassignation schedulesetid is not equal to schedulesetid.
          //if already assigned one is other than the current one
        if (prevassign.get('scheduleset.id') !== scheduleset.id){//if prev assign and of another ssid
var confirm = window.confirm("Another Scheduleset("+prevassign.get('scheduleset.id')+") already assigned to this day. Would you like to overwrite?");
if (confirm) {

          prevassign.deleteRecord();
          prevassign.save().then(function(){
            let newAssign = controller.store.createRecord('assignation',{
              scheduleset:scheduleset,
              day:day
            });
            newAssign.save().then(function(){
              controller.set('buttonDisabled',false);
            });

          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong !' ,
              type: 'error',
              autoClear: true
            });
          });

        }else{
          controller.set('buttonDisabled',false);
        }
        }else{// if no previous assignation on same day // un assign if the button is double clicked
          prevassign.deleteRecord();
          prevassign.save().then(function(){
            controller.set('buttonDisabled',false);
          });

        }


      }else{// if no assignaion is done yet
        let newAssign = controller.store.createRecord('assignation',{
          scheduleset:scheduleset,
          day:day
        });
        newAssign.save().then(function(){
          controller.set('buttonDisabled',false);
        });

      }
    });
  }else{
    controller.notifications.addNotification({
      message:  'Add timings before assigning !' ,
      type: 'error',
      autoClear: true
    });
  }
  },

  deleteTiming:function(timing){
    var controller = this;

    controller.set('buttonDisabled',true);

    timing.destroyRecord().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){

      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },

  addTiming:function(scheduleset){
    var controller = this;

      controller.set('buttonDisabled',true);
        var schedulesetTimings = scheduleset.get('scheduleset');
        var schedulesettimngslength =  scheduleset.get('timings.length');
        console.log("timing_count"+schedulesettimngslength);


      console.log("hi"+schedulesetTimings);
      var newtime ;
      var defaultaudio = controller.store.peekRecord('audio',1);
      var selectzone;
      var hexavalue;
      var zoness,activezones="";
      controller.set('editTiming',false);

    if(schedulesettimngslength === 0 ){


      // if no timings new time is 8 am
      newtime  =  new Date('2017-05-05 08:00:00');

      var controller = this;
      var allassignations = scheduleset.get('timing');




      var newTiming = controller.store.createRecord('timing',{
        scheduleset:scheduleset,
        time:newtime,
        audio:defaultaudio
      });

      // zone2:

      newTiming.save().then(function(){

        controller.set('buttonDisabled',false);


      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong!' ,
          type: 'error',
          autoClear: true
        });
      });
    }else{
      var lastTime = controller.get('schedulesetTimings').get('lastObject').get('time');

      // here add the logic to stop by 23:59 pm

      newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
      if(newtime >  new Date("January 01, 2000 23:30:00"))
      {
        newtime  = new Date(lastTime.getTime() + 1*60000);
      }

         var schedulesetTimings = controller.get('schedulesetTimings');
        var schedulesetTimingsLength = controller.get('schedulesetTimings.length');


        var schedulesetTimingsprocessed = 0;
        schedulesetTimings.forEach(function(timing) {
  // if(timing.get('editTiming') === true){




        var equalcounter = 0;
        schedulesetTimings.forEach(function(schedulesettiming) {
            // To check if Duplicate timings exists

          if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
            equalcounter++;
           }
        });
        if(equalcounter < 2 ){// if same timing dont appear more than once.
          if(timing.get('editTiming') === true){
          timing.set('editTiming',false);
          timing.save().then(function(){
            controller.set('buttonDisabled',false);
          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong!' ,
              type: 'error',
              autoClear: true
            });
          });
  }

        }else{
          controller.set('buttonDisabled',false);
           controller.notifications.addNotification({
            message:  'Duplicate Times!' ,
            type: 'error',
            autoClear: true
          });
        }

        schedulesetTimingsprocessed++;
        if(schedulesetTimingsprocessed === schedulesetTimingsLength){
          var newTiming = controller.store.createRecord('timing',{
            scheduleset:scheduleset,
            time:newtime,
            audio:defaultaudio,
            zones:hexavalue,
            activezones:activezones
          });
          newTiming.save().then(function(){
            newTiming.set('editTiming',true);


            controller.set('buttonDisabled',false);
          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong!' ,
              type: 'error',
              autoClear: true
            });
          });
        }
      // }
      });
    }


    },
  
  editTiming:function(timing){
    timing.set('editTiming',true);
  },

  zone1toggle:function(timing)
  {
    timing.toggleProperty('zone1');
    // timing.zone1 = true;
  },
  
  zone2toggle:function(timing)
  {
    timing.toggleProperty('zone2');

  },
  zone3toggle:function(timing)
  {
    timing.toggleProperty('zone3');

  },
  zone4toggle:function(timing)
  {
    timing.toggleProperty('zone4');

  },

saveAndAddAnother:function(timing ){

  var controller = this;
  var defaultaudio;
  controller.set('buttonDisabled',true);
  var scheduleset = controller.get('scheduleset');

  var schedulesetTimings = controller.get('schedulesetTimings');
  var equalcounter = 0;
  schedulesetTimings.forEach(function(schedulesettiming) {

    if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
      equalcounter++;
    }
  });
  if(equalcounter < 2 ){

    timing.set('editTiming',false);
    timing.save().then(function(){
      controller.set('buttonDisabled',false);
       controller.set('buttonDisabled',true);
      var schedulesettimngslength =  scheduleset.get('timings.length');
      var newtime ;
    if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
    defaultaudio  = controller.store.peekRecord('audio',1);
      newtime  =  new Date('2017-05-05 08:00:00');
    }else{

      var lastTime = controller.get('schedulesetTimings').get('lastObject').get('time');
      defaultaudio = controller.get('schedulesetTimings').get('lastObject').get('audio');
      newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
      if(newtime >  new Date("January 01, 2000 23:30:00"))
      {
        newtime  = new Date(lastTime.getTime() + 1*60000);
      }
    }


      var newTiming = controller.store.createRecord('timing',{
        scheduleset:scheduleset,
        time:newtime,
        audio:defaultaudio

      });
      newTiming.save().then(function(newTiming){
        controller.set('buttonDisabled',false);
        newTiming.set('editTiming',true);
      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong!' ,
          type: 'error',
          autoClear: true
        });
      });


    }).catch(function(){
      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });


  }else{
    controller.notifications.addNotification({
      message:  'Duplicate Times!' ,
      type: 'error',
      autoClear: true
    });
  }

},
  saveTiming:function(timing){
    var controller = this;
    controller.set('buttonDisabled',true);

    
    var schedulesetTimings = controller.get('schedulesetTimings');
    var equalcounter = 0;
    schedulesetTimings.forEach(function(schedulesettiming) {
        // To check if Duplicate timings exists
      if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
        equalcounter++;
      }
    });
    if(equalcounter < 2 ){

      timing.set('editTiming',false);
      timing.save().then(function(){
        controller.set('buttonDisabled',false);
      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong!' ,
          type: 'error',
          autoClear: true
        });
      });


    }else{
      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Duplicate Times!' ,
        type: 'error',
        autoClear: true
      });
    }

  },

  selectFile:function(audio) {
    console.log(audio.get('filename'));
  },
  selectHour:function(hour) {
    console.log(hour);
  }
}
});