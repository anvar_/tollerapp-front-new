import Ember from 'ember';

export default Ember.Controller.extend({


  isSaveConfigDisabled:Ember.computed('user.dyndns','user.internetport',function(){
    if(Ember.isEmpty(this.get('user.dyndns')) ||
    Ember.isEmpty(this.get('user.internetport'))
  ){return true;}
  else{return false;}
}),


isSaveEtherNetConfigDisabled:Ember.computed('user.ethernetport',function(){

  return Ember.isEmpty(this.get('user.ethernetport')) ;

}),


actions:{
  SaveUser:function(user){
    var controller = this;
    controller.set('isSaveConfigDisabled',true);
    controller.set('isSaveEtherNetConfigDisabled',true);

    user.save().then(function(){
      controller.set('isSaveConfigDisabled',false);
      controller.set('isSaveEtherNetConfigDisabled',false);
    }).catch(function(){
      controller.set('isSaveConfigDisabled',false);
      controller.set('isSaveEtherNetConfigDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

  }
}
});
