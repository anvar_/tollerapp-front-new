
import Ember from 'ember';
import EmberUploader from 'ember-uploader';
import ENV from '../../../config/environment';





export default Ember.Controller.extend({






  session: Ember.inject.service('session'),
  buttonDisabled:false,

  uploadPercent:0,

actions:{
deleteAudio:function(audio){
  var controller = this;
  controller.set('buttonDisabled',true);
  audio.destroyRecord().then(function(){
controller.set('buttonDisabled',false);
  }).catch(function(){
    controller.set('buttonDisabled',false);
    controller.notifications.addNotification({
      message:  'Oops ! Looks like you have assigned this file to some scheduleset.' ,
      type: 'error',
      autoClear: true
    });

  });
},

uploadProfilePic :function(params){
  var controller = this;


  var authenticated = controller.get('session.data.authenticated');
  let files = params.files,
  user = params.user;

  var newAudio = controller.store.createRecord('quicknotification',{description: '',user :user});

  newAudio.save().then(function(newDesignImage){

    var uploader = EmberUploader.Uploader.extend({
      url: ENV.APP.host + '/quicknotifications/'+newDesignImage.id,
      method: 'PATCH',
      paramNamespace: 'quicknotification',
      paramName: 'url',
      ajaxSettings: {
        headers: {
          'Authorization':'Token token="'+ authenticated.token +'", username="'+ authenticated.username +'"'
        }
      },
    }).create();

    uploader.on('progress', function(e) {
      controller.set('uploadPercent',e.percent);
    });

    uploader.on('didUpload', function() {

      controller.set('uploadPercent',0);
      controller.notifications.addNotification({
        message:  'File uploaded' ,
        type: 'success',
        autoClear: true
      });
      controller.send('reloadModel');//TODO: ADD preload="none" to all audio controls

    });

    uploader.on('didError', function(jqXHR, textStatus, errorThrown) {
      console.log(jqXHR,textStatus,errorThrown);
      controller.notifications.addNotification({
        message: 'Sorry something went wrong' ,
        type: 'error',
        autoClear: true
      });
    });


    if (!Ember.isEmpty(files)) {
      if(files[0].size > 1110000){
        newAudio.destroyRecord();
        controller.notifications.addNotification({
          message: 'Sorry File size is greater than 1M' ,
          type: 'error',
          autoClear: true
        });
      }else{
        uploader.upload(files[0]).then(function(){

        }
      ).catch(function(){
        controller.notifications.addNotification({
          message: 'Sorry something went wrong' ,
          type: 'error',
          autoClear: true
        });
      });

      }

  }

});

},


}


});
