import Ember from 'ember';
import EmberUploader from 'ember-uploader';
import ENV from '../../../../config/environment';

export default Ember.Controller.extend({

  session: Ember.inject.service('session'),



editAnn:false,

currentUserrole:Ember.computed('session',function(){
  return this.get('session.data.role');
}),

actions:{





  editDescription:function(ticket){
    ticket.set('editdescription',true);
  },


  saveDescription:function(ticket){
    var controller = this;

    controller.set("buttonDisabled",true);
    ticket.save().then(function(){
    ticket.set('editdescription',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    ticket.set('editdescription',false);
    controller.set("buttonDisabled",false);
  });

  },




  editBody:function(ticket){
    ticket.set('editbody',true);
  },


  saveBody:function(ticket){
    var controller = this;

    controller.set("buttonDisabled",true);
    ticket.save().then(function(){
    ticket.set('editbody',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    ticket.set('editbody',false);
    controller.set("buttonDisabled",false);
  });

  },

  editReply:function(reply){
    reply.set('editreply',true);
  },


deleteAttachment:function(attachment){
  var controller = this;

  controller.set('buttonDisabled',true);

  attachment.destroyRecord().then(function(){

    controller.set('buttonDisabled',false);

  }).catch(function(){

    controller.set('buttonDisabled',false);
    controller.notifications.addNotification({
      message:  'Something Went Wrong!' ,
      type: 'error',
      autoClear: true
    });
  });


},



  deleteReply:function(reply){
    var controller = this;

    controller.set('buttonDisabled',true);

    reply.destroyRecord().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){

      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },

  saveReply:function(reply){
    var controller = this;
    if(!Ember.isEmpty(reply.get('body'))){



    controller.set("buttonDisabled",true);
    reply.save().then(function(){
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  });
}else{
  controller.notifications.addNotification({
    message:  'Empty Reply!' ,
    type: 'error',
    autoClear: true
  });
}

  },

  addReply:function(ticket){
    var controller = this;
    controller.set('buttonDisabled' ,true);

    var user = controller.store.findRecord('user', controller.get('session.data.user_id'));

    user.then(function(){
      var newReply = controller.store.createRecord('reply',
    {
      ticket:ticket,
      user:user,
    });

    newReply.save().then(function(newReply){
      newReply.set('editreply',true);

      controller.set('buttonDisabled' ,false);
    }).catch(function(){
      controller.set('buttonDisabled' ,false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

});


  },



  SaveTicket:function(ticket){
    var controller = this;
    controller.set('isCreateTicketDisabled' ,true);
    ticket.save().then(function(){
      controller.set('isCreateTicketDisabled' ,false);
      controller.transitionToRoute('dashboard.local.tickets.index');
    }).catch(function(){
      controller.set('isCreateTicketDisabled' ,false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

  }
}

});
