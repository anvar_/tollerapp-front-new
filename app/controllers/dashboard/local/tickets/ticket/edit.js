import Ember from 'ember';
import EmberUploader from 'ember-uploader';
import ENV from '../../../../../config/environment';

export default Ember.Controller.extend({

  session: Ember.inject.service('session'),

  isCreateTicketDisabled:Ember.computed('ticket.title','ticket.body',function(){
    if(Ember.isEmpty(this.get('ticket.title')) ||
    Ember.isEmpty(this.get('ticket.body'))
  ){return true;}
  else{return false;}
}),

buttonDisabled:false,

actions:{





  uploadAttachment :function(params){
     var controller = this;

     var authenticated = controller.get('session.data.authenticated');
    var files = params.files;
     var ticket = params.ticket;

     var newTicket = controller.store.createRecord('attachment',{
       ticket:ticket,
     });


     newTicket.save().then(function(newAttachment){

     var uploader = EmberUploader.Uploader.extend({
       url: ENV.APP.host + '/attachments/'+newAttachment.id,
       method: 'PATCH',
       paramNamespace: 'attachment',
       paramName: 'url',
       ajaxSettings: {
         headers: {
           'Authorization':'Token token="'+ authenticated.token +'", username="'+ authenticated.username +'"'
         }
       },
     }).create();

     uploader.on('progress', function(e) {
       controller.set('uploadPercent',e.percent);
     });

     uploader.on('didUpload', function() {
       controller.set('uploadPercent',0);

       controller.notifications.addNotification({
         message:  'File uploaded' ,
         type: 'success',
         autoClear: true
       });
       controller.send('reloadModel');
      //  controller.transitionToRoute('dashboard.local.tickets.ticket.edit', ticket);


     });

     uploader.on('didError', function(jqXHR, textStatus, errorThrown) {
       console.log(jqXHR,textStatus,errorThrown);
       controller.notifications.addNotification({
         message: 'Sorry something went wrong' ,
         type: 'success',
         autoClear: true
       });
     });


     if (!Ember.isEmpty(files)) {
       if(files[0].size > 2110000){
         newAttachment.destroyRecord();
         controller.notifications.addNotification({
           message: 'Sorry File size is greater than 2M' ,
           type: 'error',
           autoClear: true
         });
       }else{
         uploader.upload(files[0]).then(function(){

         }
       ).catch(function(){
         controller.notifications.addNotification({
           message: 'Sorry something went wrong' ,
           type: 'error',
           autoClear: true
         });
       });

       }

   }


});


 },


closeTicket(ticket){
  var controller  = this;
  controller.set("buttonDisabled",true);
  ticket.set('status','closed');
  ticket.save().then(function(){
  controller.set("buttonDisabled",false);
}).catch(function(){
  controller.notifications.addNotification({
    message:  'Something Went Wrong !' ,
    type: 'error',
    autoClear: true
  });
  controller.set("buttonDisabled",false);
});


},

reOpenTicket(ticket){
  var controller  = this;


  controller.set("buttonDisabled",true);
  ticket.set('status','open');
  ticket.save().then(function(){
  controller.set("buttonDisabled",false);
}).catch(function(){
  controller.notifications.addNotification({
    message:  'Something Went Wrong !' ,
    type: 'error',
    autoClear: true
  });
  controller.set("buttonDisabled",false);
});

},

  editDescription:function(ticket){
    ticket.set('editdescription',true);
  },


  saveDescription:function(ticket){
    var controller = this;

    controller.set("buttonDisabled",true);
    ticket.save().then(function(){
    ticket.set('editdescription',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    ticket.set('editdescription',false);
    controller.set("buttonDisabled",false);
  });

  },




  editBody:function(ticket){
    ticket.set('editbody',true);
  },


  saveBody:function(ticket){
    var controller = this;

    controller.set("buttonDisabled",true);
    ticket.save().then(function(){
    ticket.set('editbody',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    ticket.set('editbody',false);
    controller.set("buttonDisabled",false);
  });

  },

  editReply:function(reply){
    reply.set('editreply',true);
  },


deleteAttachment:function(attachment){
  var controller = this;

  controller.set('buttonDisabled',true);

  attachment.destroyRecord().then(function(){

    controller.set('buttonDisabled',false);

  }).catch(function(){

    controller.set('buttonDisabled',false);
    controller.notifications.addNotification({
      message:  'Something Went Wrong!' ,
      type: 'error',
      autoClear: true
    });
  });


},



  deleteReply:function(reply){
    var controller = this;

    controller.set('buttonDisabled',true);

    reply.destroyRecord().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){

      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },

  saveReply:function(reply){
    var controller = this;
    if(!Ember.isEmpty(reply.get('body'))){



    controller.set("buttonDisabled",true);
    reply.save().then(function(){
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  });
}else{
  controller.notifications.addNotification({
    message:  'Empty Reply!' ,
    type: 'error',
    autoClear: true
  });
}

  },

  addReply:function(ticket){
    var controller = this;
    controller.set('buttonDisabled' ,true);

    var user = controller.store.findRecord('user', controller.get('session.data.user_id'));

    user.then(function(){
      var newReply = controller.store.createRecord('reply',
    {
      ticket:ticket,
      user:user,
      status:'open'
    });

    newReply.save().then(function(newReply){
      newReply.set('editreply',true);

      controller.set('buttonDisabled' ,false);
    }).catch(function(){
      controller.set('buttonDisabled' ,false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

});


  },



  SaveTicket:function(ticket){
    var controller = this;
    controller.set('isCreateTicketDisabled' ,true);
    ticket.save().then(function(){
      controller.set('isCreateTicketDisabled' ,false);
      controller.transitionToRoute('dashboard.local.tickets.index');
    }).catch(function(){
      controller.set('isCreateTicketDisabled' ,false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

  }
}

});
