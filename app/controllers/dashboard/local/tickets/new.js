import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),

userId: Ember.computed('session', function() {
	  return (this.get('session.data.user_id'));
	}),

  isCreateTicketDisabled:Ember.computed('title','body',function(){
    if(Ember.isEmpty(this.get('title')) ||
    Ember.isEmpty(this.get('body'))
  ){return true;}
  else{return false;}
}),





    actions:{
      createTicket:function(){
        var controller = this;
        controller.set('isCreateTicketDisabled' ,true);

        
        var user = controller.store.findRecord('user', parseInt(controller.get('userId')));


        user.then(function(){



          var newTicket = controller.store.createRecord('ticket',
        {
          title:controller.get('title'),
          body:controller.get('body'),
          user:user,
          status:'open'
        });

        newTicket.save().then(function(){
          controller.set('isCreateTicketDisabled' ,false);
          controller.transitionToRoute('dashboard.local.tickets.index');
        }).catch(function(){
          controller.set('isCreateTicketDisabled' ,false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong !' ,
            type: 'error',
            autoClear: true
          });
        });

        });


      }
    }
});
