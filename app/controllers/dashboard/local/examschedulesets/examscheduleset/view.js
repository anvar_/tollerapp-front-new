import Ember from 'ember';

export default Ember.Controller.extend({


  // to populate copy scheduleset list except current scheduleset
  routeuserexamschedulesets:Ember.computed('userexamschedulesets','examscheduleset',function(){

    var examscheduleset = this.get('examscheduleset');
    var userexamschedulesets = this.get('userexamschedulesets');

    return userexamschedulesets.filter(function(item){
      return item.get('id') !== examscheduleset.get('id');
    });
  }),

  assignationDisabled:Ember.computed('sortedExamtimings',function(){
    var schedulesetTimingslength = this.get('sortedExamtimings.length');
    var schedulesetAssignations = this.get('examscheduleset').get('examassignations');
    if(schedulesetTimingslength===0){
      schedulesetAssignations.then(function(assignations){
        assignations.forEach(function(assignation){
          assignation.destroyRecord();
        });
      });
      return 'disabled';
    }else{
      return '';
    }
  }),

buttonDisabled:false,

  sortProperties: ['time:asc'],
  sortPropertiestimings: ['time:asc'],

  sortedExamassignations: Ember.computed.sort('examscheduleset.examassignations', 'sortProperties'),
  sortedExamtimings: Ember.computed.sort('examscheduleset.examtimings', 'sortPropertiestimings'),

  maxassignation:24,

  sortedExamassignationsCount: Ember.computed.alias('examscheduleset.examassignations.length'),
  sortedExamtimingsCount: Ember.computed.alias('examscheduleset.examtimings.length'),



  actions:{


    unAssignall:function(scheduleset){
      var controller = this;
      var allassignations = scheduleset.get('examassignations');
      allassignations.then(function(allassignations){

        var allassignationsprocessed = 0;
      var allassignationslength = scheduleset.get('examassignations.length');
      allassignations.forEach(function(assignation){
        assignation.destroyRecord().then(function(){
          allassignationsprocessed++;
          if(allassignationsprocessed === allassignationslength){
            controller.notifications.addNotification({
                message:  'All assignations deleted !' ,
                type: 'success',
                autoClear: true
              });
          }
        }).catch(function(){
            controller.notifications.addNotification({
                message:  'Something Went Wrong !' ,
                type: 'error',
                autoClear: true
              });
              controller.set('buttonDisabled',false);
        });
      });
      });

    },

    deleteAllTimings:function(scheduleset){
      var controller = this;
      controller.set('buttonDisabled',true);

      var schedulesetTimings = scheduleset.get('examtimings');
      var schedulesetTimingsprocessed = 0;
      var schedulesetTimingslength = scheduleset.get('examtimings.length');

      schedulesetTimings.forEach(function(schedulesetTiming){
        schedulesetTiming.destroyRecord().then(function(){
          schedulesetTimingsprocessed++;
          if(schedulesetTimingsprocessed ===  schedulesetTimingslength){
            controller.notifications.addNotification({
              message:  'Timings deleted !' ,
              type: 'success',
              autoClear: true
            });
            controller.set('buttonDisabled',false);
          }

        }).catch(function(){
          controller.notifications.addNotification({
            message:  'Something Went Wrong !' ,
            type: 'error',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
        });

      });
    },



    editDescription:function(examscheduleset){
      examscheduleset.set('editdescription',true);
    },


    saveDescription:function(examscheduleset){
      var controller = this;

      controller.set("buttonDisabled",true);
      examscheduleset.save().then(function(){
      examscheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      examscheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    });
  },



    copySuperScheduleset:function(){
      var controller = this;
      controller.notifications.addNotification({
        message:  'Copying.... Please Wait.' ,
        type: 'info',
      });
      controller.set('buttonDisabled',true);


      var examscheduleset = controller.get('examscheduleset'); //current scheduleset
      var superuserexamscheduleset = controller.store.findRecord('examscheduleset',1);// this is static
      superuserexamscheduleset.then(function(superuserexamscheduleset){
      var timingitemsProcessed = 0;
      var timinglength = examscheduleset.get('examtimings.length');
      var supertimingitemsProcessed = 0;
      var supertiminglength = superuserexamscheduleset.get('examtimings.length');
      if(supertiminglength === 0 ){
        controller.notifications.clearAll();
        controller.notifications.addNotification({
          message:  'Timings are empty' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);

    }else{

      if(timinglength===0){



                              var timings = superuserexamscheduleset.get('examtimings');

                              timings.then(function(timings){
                                timings.forEach(function(usertiming){
                                  var time = usertiming.get('time');
                                  var audio = usertiming.get('audio');
                                  var newTiming =   controller.store.createRecord('examtiming',{
                                    time:time,
                                    examscheduleset:examscheduleset,
                                    audio:audio

                                  });
                                  newTiming.save().then(function(){
                                    supertimingitemsProcessed++;
                                    if(supertimingitemsProcessed === supertiminglength){
                                      controller.notifications.clearAll();
                                      controller.notifications.addNotification({
                                        message:  'Scheduleset Copied' ,
                                        type: 'success',
                                        autoClear: true
                                      });
                                      controller.set('buttonDisabled',false);

                                    }
                                  });
                                });
                              });


      }else{

              examscheduleset.get('examtimings').forEach(function(timing) {
                timing.destroyRecord().then(function(){
                  timingitemsProcessed++ ;
                  if(timingitemsProcessed === timinglength ){
                      var timings = superuserexamscheduleset.get('examtimings');

                              timings.then(function(timings){
                                timings.forEach(function(usertiming){
                                  var time = usertiming.get('time');
                                  var newTiming =   controller.store.createRecord('examtiming',{
                                    time:time,
                                    examscheduleset:examscheduleset
                                  });
                                  newTiming.save().then(function(){
                                    supertimingitemsProcessed++;
                                    if(supertimingitemsProcessed === supertiminglength){
                                      controller.notifications.clearAll();
                                      controller.notifications.addNotification({
                                        message:  'Scheduleset Copied' ,
                                        type: 'success',
                                        autoClear: true
                                      });
                                      controller.set('buttonDisabled',false);

                                    }
                                  });
                                });
                              });

                  }

                });
              });

      }
    }
  });











    },





    copyOtherScheduleset:function(){
      var controller = this;
      var selecteduserexamscheduleset = this.get('userexamscheduleset');//selected scheduleset
      var currentexamscheduleset = this.get('examscheduleset'); //current scheduleset

      if(selecteduserexamscheduleset){

        controller.notifications.addNotification({
          message:  'Copying.... Please Wait.' ,
          type: 'info',
        });
        controller.set('buttonDisabled',true);


        var timingitemsProcessed = 0;
        var currentschedulesettimingslength = currentexamscheduleset.get('examtimings.length');
        var selectedschedulesettimingslength = selecteduserexamscheduleset.get('examtimings.length');
        var timings = selecteduserexamscheduleset.get('examtimings');

        if(selectedschedulesettimingslength===0){
          controller.notifications.clearAll();
          controller.notifications.addNotification({
            message:  'Timings are empty' ,
            type: 'error',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
        }else{



        if(currentschedulesettimingslength===0){
          var selectedtimingitemsProcessed = 0;

          timings.then(function(timings){
            timings.forEach(function(usertiming){
              var time = usertiming.get('time');
              var newTiming =   controller.store.createRecord('examtiming',{
                time:time,
                examscheduleset:currentexamscheduleset
              });
              newTiming.save().then(function(){
                selectedtimingitemsProcessed++;
                if(selectedtimingitemsProcessed === selectedschedulesettimingslength){
                  controller.notifications.clearAll();
                  controller.notifications.addNotification({
                    message:  'Scheduleset Copied' ,
                    type: 'success',
                    autoClear: true
                  });
                  controller.set('buttonDisabled',false);
                }
              });
            });
          });
        }else{
          currentexamscheduleset.get('examtimings').forEach(function(timing) {
            timing.destroyRecord().then(function(){
              timingitemsProcessed++;
              if(timingitemsProcessed === currentschedulesettimingslength){


                var selectedtimingitemsProcessed = 0;


                timings.then(function(timings){
                  timings.forEach(function(usertiming){
                    var time = usertiming.get('time');
                    var newTiming =   controller.store.createRecord('examtiming',{
                      time:time,
                      examscheduleset:currentexamscheduleset
                    });
                    newTiming.save().then(function(){
                      selectedtimingitemsProcessed++;
                      if(selectedtimingitemsProcessed === selectedschedulesettimingslength){
                        controller.notifications.clearAll();
                        controller.notifications.addNotification({
                          message:  'Scheduleset Copied' ,
                          type: 'success',
                          autoClear: true
                        });
                        controller.set('buttonDisabled',false);
                      }
                    });
                  });
                });



              }
            });
          });
        }
      }


      }else{
        controller.notifications.addNotification({
          message:  'Please select a scheduleset' ,
          type: 'warning',
          autoClear: true
        });
      }

    },


    saveAssignationDate:function(examassignation , value){
      var controller = this;
      console.log(value);



      var diff = value.getTimezoneOffset();
      value.setHours(0);
      value.setMinutes(0);
      value.setSeconds(0);
      value = new Date(value.getTime() - diff*60000);
      // console.log(value)

      examassignation.set('time',value);

      examassignation.save().then(function(){
        controller.notifications.addNotification({
          message:  'Saved' ,
          type: 'success',
          autoClear: true
        });
      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
      });
    },



    addAssignation:function(examscheduleset){



      var controller = this;
      if(controller.get('sortedExamtimings.length')!==0){
        var examschedulesetAssignationslength =  controller.get('sortedExamassignations.length');
        if(examschedulesetAssignationslength === 0 ){


      controller.set('buttonDisabled',true);

        /*

          Mysql saves datetime after converting incoming datetime into utc.
          Inorder to compare dates the times saved at database should be 00:00:00 .
          So Inorder to save the time at utc as 00:00:00,
          we should substract the  current time (in Minutes) with the timezone offset which is in minutes.
          And send the data as it is.

        */
        var date = new Date();


        var diff = date.getTimezoneOffset();
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        var newDateObj = new Date(date.getTime() - diff*60000);

      var newAssignation = controller.store.createRecord('examassignation',{

        examscheduleset:examscheduleset,
        time: newDateObj,

      });

      newAssignation.save().then(function(){
        controller.set('buttonDisabled',false);
      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });
    }else{
      var lastAssignationTime = controller.get('sortedExamassignations').get('lastObject').get('time');
      var date = lastAssignationTime;
      var diff = date.getTimezoneOffset();

      date.setHours(0);
      date.setMinutes(0);
      date.setSeconds(0);
      date.setDate(date.getDate() + 1);

      var newDateObj = new Date(date.getTime() - diff*60000);


    var newAssignation = controller.store.createRecord('examassignation',{

      examscheduleset:examscheduleset,
      time: newDateObj,

    });
    newAssignation.save().then(function(){
      controller.set('buttonDisabled',false);
    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      controller.set('buttonDisabled',false);
    });
    }


    }else
    {
      controller.notifications.addNotification({
        message:  'Add timings before assigning !' ,
        type: 'error',
        autoClear: true
      });
    }
    },



    deleteAssignation:function(examassignation){
      var controller = this;
      controller.set('buttonDisabled',true);

      examassignation.destroyRecord().then(function(){
        controller.notifications.addNotification({
          message:  'Deleted' ,
          type: 'success',
          autoClear: true
        });
        controller.set('buttonDisabled',false);

      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });

    },


    deleteTiming:function(timing){
      var controller = this;
      controller.set('buttonDisabled',true);

      timing.destroyRecord().then(function(){

        controller.set('buttonDisabled',false);

      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });
    },

    addTiming:function(examscheduleset){
      var controller = this;
      controller.set('buttonDisabled',true);
      var schedulesettimngslength =  examscheduleset.get('examtimings.length');
      var newtime ;
      var defaultaudio = controller.store.peekRecord('audio',1);

    if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
      newtime  =  new Date('2017-05-05 08:00:00');
      var newTiming = controller.store.createRecord('examtiming',{
        examscheduleset:examscheduleset,
        time:newtime,
        audio:defaultaudio
      });

      newTiming.save().then(function(){

        controller.set('buttonDisabled',false);

      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });
    }else{
      var lastTime = controller.get('sortedExamtimings').get('lastObject').get('time');
      newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
      if(newtime >  new Date("January 01, 2000 23:30:00"))
      {
        newtime  = new Date(lastTime.getTime() + 1*60000);
      }
      var schedulesetTimings = controller.get('sortedExamtimings');
     var schedulesetTimingsLength = controller.get('sortedExamtimings.length');
     var schedulesetTimingsprocessed = 0;
schedulesetTimings.forEach(function(timing) {


  var equalcounter = 0;

     schedulesetTimings.forEach(function(schedulesettiming) {
       if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
         equalcounter++;
        }
     });
     if(equalcounter < 2 ){// if same timing dont appear more than once.
if(timing.get('editTiming') === true){
       timing.set('editTiming',false);
       timing.save().then(function(){
         controller.set('buttonDisabled',false);
       }).catch(function(){
         controller.set('buttonDisabled',false);
         controller.notifications.addNotification({
           message:  'Something Went Wrong!' ,
           type: 'error',
           autoClear: true
         });
       });
}

     }else{
       controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
         message:  'Duplicate Times!' ,
         type: 'error',
         autoClear: true
       });
     }
     schedulesetTimingsprocessed++;

     if(schedulesetTimingsprocessed === schedulesetTimingsLength){
       var newTiming = controller.store.createRecord('examtiming',{
         examscheduleset:examscheduleset,
         time:newtime,
         audio:defaultaudio
       });

       newTiming.save().then(function(){

         controller.set('buttonDisabled',false);

       }).catch(function(){
         controller.notifications.addNotification({
           message:  'Something Went Wrong !' ,
           type: 'error',
           autoClear: true
         });
         controller.set('buttonDisabled',false);
       });
     }


});



    }


    },




    editTiming:function(timing){
      timing.set('editTiming',true);
    },


saveAndAddAnother:function(timing ){

  var controller = this;
  var examscheduleset= controller.get('examscheduleset');
  controller.set('buttonDisabled',true);

  var examschedulesetTimings = controller.get('sortedExamtimings');
  var equalcounter = 0;


  examschedulesetTimings.forEach(function(schedulesettiming) {
    if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
      equalcounter++;
    }
  });


  if(equalcounter < 2 ){

    timing.set('editTiming',false);
    timing.save().then(function(){
      controller.set('buttonDisabled',false);


////
controller.set('buttonDisabled',true);
var schedulesettimngslength =  examscheduleset.get('examtimings.length');
var newtime ;
if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
newtime  =  new Date('2017-05-05 08:00:00');
}else{
var lastTime = controller.get('sortedExamtimings').get('lastObject').get('time');
newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
if(newtime >  new Date("January 01, 2000 23:30:00"))
{
  newtime  = new Date(lastTime.getTime() + 1*60000);
}
}
var defaultaudio = controller.store.peekRecord('audio',1);
var newTiming = controller.store.createRecord('examtiming',{
  examscheduleset:examscheduleset,
  time:newtime,
  audio:defaultaudio
});

newTiming.save().then(function(){

  controller.set('buttonDisabled',false);

}).catch(function(){
  controller.notifications.addNotification({
    message:  'Something Went Wrong !' ,
    type: 'error',
    autoClear: true
  });
  controller.set('buttonDisabled',false);
});
////




    }).catch(function(){
      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });


  }else{
    controller.notifications.addNotification({
      message:  'Duplicate Times!' ,
      type: 'error',
      autoClear: true
    });
  }
},
    saveTiming:function(timing){
      var controller = this;
      controller.set('buttonDisabled',true);

      var examschedulesetTimings = controller.get('sortedExamtimings');
      var equalcounter = 0;


      examschedulesetTimings.forEach(function(schedulesettiming) {
        if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
          equalcounter++;
        }
      });


      if(equalcounter < 2 ){

        timing.set('editTiming',false);
        timing.save().then(function(){
          controller.set('buttonDisabled',false);
        }).catch(function(){
          controller.set('buttonDisabled',false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong!' ,
            type: 'error',
            autoClear: true
          });
        });


      }else{
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Duplicate Times!' ,
          type: 'error',
          autoClear: true
        });
      }



    },





  }
});
