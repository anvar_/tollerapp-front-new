import Ember from 'ember';

export default Ember.Controller.extend({
session: Ember.inject.service('session'),
buttonDisabled:false,

currentUserrole:Ember.computed('session',function(){
  return this.get('session.data.role');
}),

actions:{


  closeTicket(ticket){
    var controller  = this;
    controller.set("buttonDisabled",true);
    ticket.set('status','closed');
    ticket.save().then(function(){
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    controller.set("buttonDisabled",false);
  });


  },

  reOpenTicket(ticket){
    var controller  = this;


    controller.set("buttonDisabled",true);
    ticket.set('status','open');
    ticket.save().then(function(){
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    controller.set("buttonDisabled",false);
  });

  },


  editReply:function(reply){
    reply.set('editreply',true);
  },


  deleteReply:function(reply){
    var controller = this;

    controller.set('buttonDisabled',true);

    reply.destroyRecord().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){

      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },

  saveReply:function(reply){
    var controller = this;
    if(!Ember.isEmpty(reply.get('body'))){



    controller.set("buttonDisabled",true);
    reply.save().then(function(){
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  }).catch(function(){
    controller.notifications.addNotification({
      message:  'Something Went Wrong !' ,
      type: 'error',
      autoClear: true
    });
    reply.set('editreply',false);
    controller.set("buttonDisabled",false);
  });
}else{
  controller.notifications.addNotification({
    message:  'Empty Reply!' ,
    type: 'error',
    autoClear: true
  });
}

  },

  addReply:function(ticket){
    var controller = this;
    controller.set('buttonDisabled' ,true);

    var user = controller.store.findRecord('user', controller.get('session.data.user_id'));

    user.then(function(){
      var newReply = controller.store.createRecord('reply',
    {
      ticket:ticket,
      user:user,
    });

    newReply.save().then(function(newReply){
      newReply.set('editreply',true);

      controller.set('buttonDisabled' ,false);
    }).catch(function(){
      controller.set('buttonDisabled' ,false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });

});


  },


}

});
