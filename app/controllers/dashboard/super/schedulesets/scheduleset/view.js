import Ember from 'ember';

export default Ember.Controller.extend({

session: Ember.inject.service('session'),

  buttonDisabled:false,
  sortPropertiestimings: ['time:asc'],

    schedulesetTimings: Ember.computed.sort('scheduleset.timings', 'sortPropertiestimings'),

    schedulesetTimingsCount: Ember.computed.alias('scheduleset.timings.length'),
    maxassignation:24,



  actions:{

    editDescription:function(scheduleset){
      scheduleset.set('editdescription',true);
    },


    saveDescription:function(scheduleset){
      var controller = this;

      controller.set("buttonDisabled",true);
      scheduleset.save().then(function(){
      scheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      scheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    });

    },


    saveAssignation:function(day,scheduleset){

      // if assigned , check assignation is linked to only one scheduleset, if exist delete the assignation , and create new assignation with the scheduleset
      var controller = this;

      controller.store.query('assignation', {filter: {day: day,user:controller.get('session.data.user_id')}})
    .then(function(prevAssignation) {

        let prevassign = prevAssignation.get("firstObject");
        if(prevassign){

        if (prevassign.get('scheduleset.id') !== scheduleset.id){//if prev assign and of another ssid
        prevassign.deleteRecord();
        prevassign.save().then(function(){
          let newAssign = controller.store.createRecord('assignation',{
            scheduleset:scheduleset,
            day:day
          });
          newAssign.save();
          // scheduleset.save();

        }).catch(function(){
        });
      }else{// if no previous assignation on same day
        prevassign.deleteRecord();
        prevassign.save();
        // scheduleset.save();
      }
    }else{
      let newAssign = controller.store.createRecord('assignation',{
        scheduleset:scheduleset,
        day:day
      });
      newAssign.save();
      // scheduleset.save();
    }
    });
  },


  deleteAllTimings:function(scheduleset){
    var controller = this;
    controller.set('buttonDisabled',true);

    var schedulesetTimings = scheduleset.get('timings');
    var schedulesetTimingsprocessed = 0;
    var schedulesetTimingslength = scheduleset.get('timings.length');

    schedulesetTimings.forEach(function(schedulesetTiming){
      schedulesetTiming.destroyRecord().then(function(){
        schedulesetTimingsprocessed++;
        if(schedulesetTimingsprocessed ===  schedulesetTimingslength){
          controller.notifications.addNotification({
            message:  'Timings deleted !' ,
            type: 'success',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
        }

      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });

    });

  },

  deleteTiming:function(timing){
    var controller = this;

    controller.set('buttonDisabled',true);

    timing.destroyRecord().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){

      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },

  addTiming:function(scheduleset){
    var controller = this;

      controller.set('buttonDisabled',true);
        var schedulesetTimings = scheduleset.get('scheduleset');
        var schedulesettimngslength =  scheduleset.get('timings.length');
        console.log("timing_count"+schedulesettimngslength);


      console.log("hi"+schedulesetTimings);
      var newtime ;
      var defaultaudio = controller.store.peekRecord('audio',1);
      controller.set('editTiming',false);

      if(schedulesettimngslength === 0 ){


      // if no timings new time is 8 am
      newtime  =  new Date('2017-05-05 08:00:00');

      var controller = this;
      var allassignations = scheduleset.get('timing');



      var newTiming = controller.store.createRecord('timing',{
        scheduleset:scheduleset,
        time:newtime,
        audio:defaultaudio
      });

      // zone2:

      newTiming.save().then(function(){

        controller.set('buttonDisabled',false);


      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong!' ,
          type: 'error',
          autoClear: true
        });
      });
    }else{
      var lastTime = controller.get('schedulesetTimings').get('lastObject').get('time');

      // here add the logic to stop by 23:59 pm

      newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
      if(newtime >  new Date("January 01, 2000 23:30:00"))
      {
        newtime  = new Date(lastTime.getTime() + 1*60000);
      }

         var schedulesetTimings = controller.get('schedulesetTimings');
        var schedulesetTimingsLength = controller.get('schedulesetTimings.length');


        var schedulesetTimingsprocessed = 0;
        schedulesetTimings.forEach(function(timing) {
  // if(timing.get('editTiming') === true){




        var equalcounter = 0;
        schedulesetTimings.forEach(function(schedulesettiming) {
            // To check if Duplicate timings exists

          if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
            equalcounter++;
           }
        });
        if(equalcounter < 2 ){// if same timing dont appear more than once.
          if(timing.get('editTiming') === true){
          timing.set('editTiming',false);
          timing.save().then(function(){
            controller.set('buttonDisabled',false);
          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong!' ,
              type: 'error',
              autoClear: true
            });
          });
  }

        }else{
          controller.set('buttonDisabled',false);
           controller.notifications.addNotification({
            message:  'Duplicate Times!' ,
            type: 'error',
            autoClear: true
          });
        }

        schedulesetTimingsprocessed++;
        if(schedulesetTimingsprocessed === schedulesetTimingsLength){
          var newTiming = controller.store.createRecord('timing',{
            scheduleset:scheduleset,
            time:newtime,
            audio:defaultaudio,
          });
          newTiming.save().then(function(){
            newTiming.set('editTiming',true);


            controller.set('buttonDisabled',false);
          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong!' ,
              type: 'error',
              autoClear: true
            });
          });
        }
      // }
      });
    }


    },



    editTiming:function(timing){
timing.set('editTiming',true);
    },


    saveAndAddAnother:function(timing ){

      var controller = this;
      controller.set('buttonDisabled',true);
      var scheduleset = controller.get('scheduleset');

      var schedulesetTimings = controller.get('schedulesetTimings');
      var equalcounter = 0;
      schedulesetTimings.forEach(function(schedulesettiming) {

        if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
          equalcounter++;
        }
      });
      if(equalcounter < 2 ){

        timing.set('editTiming',false);
        timing.save().then(function(){
          controller.set('buttonDisabled',false);
           controller.set('buttonDisabled',true);
          var schedulesettimngslength =  scheduleset.get('timings.length');
          var newtime ;
        if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
          newtime  =  new Date('2017-05-05 08:00:00');
        }else{
          var lastTime = controller.get('schedulesetTimings').get('lastObject').get('time');
          newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
        }
          var defaultaudio = controller.store.peekRecord('audio',1);
          var newTiming = controller.store.createRecord('timing',{
            scheduleset:scheduleset,
            time:newtime,
            audio:defaultaudio
          });
          newTiming.save().then(function(newTiming){
      
            controller.set('buttonDisabled',false);
            newTiming.set('editTiming',true);

          }).catch(function(){
            controller.set('buttonDisabled',false);
            controller.notifications.addNotification({
              message:  'Something Went Wrong!' ,
              type: 'error',
              autoClear: true
            });
          });


        }).catch(function(){
          controller.set('buttonDisabled',false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong!' ,
            type: 'error',
            autoClear: true
          });
        });


      }else{
        controller.notifications.addNotification({
          message:  'Duplicate Times!' ,
          type: 'error',
          autoClear: true
        });
      }

    },


    saveTiming:function(timing){
      var controller = this;
      controller.set('buttonDisabled',true);

      var schedulesetTimings = controller.get('schedulesetTimings');
      var equalcounter = 0;
      schedulesetTimings.forEach(function(schedulesettiming) {

        if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
          equalcounter++;
        }
      });
      if(equalcounter < 2 ){

        timing.set('editTiming',false);
        timing.save().then(function(){
          controller.set('buttonDisabled',false);
        }).catch(function(){
          controller.set('buttonDisabled',false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong!' ,
            type: 'error',
            autoClear: true
          });
        });


      }else{
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Duplicate Times!' ,
          type: 'error',
          autoClear: true
        });
      }

    },

    selectFile:function(audio) {

      console.log(audio.get('filename'));

    },
    selectHour:function(hour) {

      console.log(hour);
    }
  }
});
