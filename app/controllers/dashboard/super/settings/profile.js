import Ember from 'ember';

export default Ember.Controller.extend({


  checkPasswordSame:Ember.computed('password','password_confirmation',function(){
    if(this.get('password') === this.get('password_confirmation')){
      return false;
    }else{
      return true;
    }
  }),

  isSaveButtonDisabled:Ember.computed('password','password_confirmation',function(){
    if(Ember.isEmpty(this.get('password'))||
    Ember.isEmpty(this.get('password_confirmation'))
  )
    {
      return true;
    }else{
      return false;
    }
  }),

  actions:{
    saveUser:function(user){
      var controller = this;
      controller.set('isSaveButtonDisabled',true);

      user.set('password',controller.get('password'));
      user.set('password_confirmation',controller.get('password_confirmation'));
      user.save().then(function(){
        controller.set('buttonDisabled',false);
         controller.notifications.addNotification({
          message:  'Password Updated' ,
          type: 'success',
          autoClear: true
        });
        // controller.transitionToRoute('dashboard.super.users.index');
      }).catch(function(){
        controller.set('isSaveButtonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
      });

    },
  }


});
