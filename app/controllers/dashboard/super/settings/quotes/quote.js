import Ember from 'ember';

export default Ember.Controller.extend({

  isCreateQuoteDisabled:Ember.computed('quotetext','author',function(){
    if(Ember.isEmpty(this.get('quotetext')) ||
    Ember.isEmpty(this.get('author'))
  ){return true;}
  else{return false;}
}),



actions:{

  createQuote:function(){

    var controller = this;
    controller.set('isCreateQuoteDisabled',true);

    var newQuote = controller.store.createRecord('quote',{
      quotetext:controller.get('quotetext'),
      author:controller.get('author'),
      status:controller.get('status')
    });

    newQuote.save().then(function(){
      controller.set('isCreateQuoteDisabled',false);
    }).catch(function(){
      controller.set('isCreateQuoteDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
    });
  },

  editQuote:function(quote){
    quote.set('isQuoteEditing',true);
    quote.save();
  },

  saveQuote:function(quote){
    quote.set('isQuoteEditing',false);
    quote.save();
  },

  deleteQuote:function(quote){
    quote.set('isQuoteEditing',false);
    quote.destroyRecord();
  }
}
});
