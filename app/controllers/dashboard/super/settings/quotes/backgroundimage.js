import Ember from 'ember';
import EmberUploader from 'ember-uploader';
import ENV from '../../../../../config/environment';


export default Ember.Controller.extend({
session: Ember.inject.service('session'),


  buttonDisabled:false,
  uploadPercent:0,


  actions:{
    uploadProfilePic :function(params){
       var controller = this;

       var authenticated = controller.get('session.data.authenticated');
       let files = params.files;

       var newBackgroundimage = controller.store.createRecord('backgroundimage',{description: ''});

       newBackgroundimage.save().then(function(newDesignImage){



       var uploader = EmberUploader.Uploader.extend({
         url: ENV.APP.host + '/backgroundimages/'+newDesignImage.id,
         method: 'PATCH',
         paramNamespace: 'backgroundimage',
         paramName: 'url',
         ajaxSettings: {
           headers: {
             'Authorization':'Token token="'+ authenticated.token +'", username="'+ authenticated.username +'"'
           }
         },

       }).create();


       uploader.on('progress', function(e) {
         controller.set('uploadPercent',e.percent);
       });

       uploader.on('didUpload', function() {
         controller.set('uploadPercent',0);
         controller.notifications.addNotification({
           message:  'File uploaded' ,
           type: 'success',
           autoClear: true
         });
         controller.send('reloadModel');
       });

       uploader.on('didError', function(jqXHR, textStatus, errorThrown) {
         console.log(jqXHR,textStatus,errorThrown);
         controller.notifications.addNotification({
           message: 'Sorry something went wrong' ,
           type: 'success',
           autoClear: true
         });
       });


       if (!Ember.isEmpty(files)) {
         uploader.upload(files[0]).then(function(){

         }
       ).catch(function(){
         controller.notifications.addNotification({
           message: 'Sorry something went wrong' ,
           type: 'error',
           autoClear: true
         });
       });
     }


  });

     //


   },



   deleteBackgroundImage(backgroundimage){
     var controller = this;
     backgroundimage.destroyRecord().then(function(){
       controller.notifications.addNotification({
         message: 'Image deleted.' ,
         type: 'success',
         autoClear: true
       });
     }).catch(function(){
       controller.notifications.addNotification({
         message: 'Something went wrong!' ,
         type: 'error',
         autoClear: true
       });
     });
   }

  }
});
