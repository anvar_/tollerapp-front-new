import Ember from 'ember';

export default Ember.Controller.extend({


  actions:{


acknowledgeGroup:function(group,groupusersprocessed,groupuserslength){

  var controller = this;
  if(groupusersprocessed === groupuserslength){
    group.destroyRecord().then(function(){
      controller.set('buttonDisabled',false);
       controller.notifications.addNotification({
        message:  'Group Deleted' ,
        type: 'success',
        autoClear: true
      });
      controller.transitionToRoute('dashboard.super.groups.index');
    }).catch(function(){
      controller.set('buttonDisabled',false);
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      controller.transitionToRoute('dashboard.super.groups.index');
    });


  }
},

deleteGroup:function(group){
  var controller = this;
  controller.set('buttonDisabled',true);

  var groupusers = group.get('users');
  groupusers.then(function(groupusers){


    var groupusersprocessed = 0;
    var groupuserslength = groupusers.get('length');
    console.log(groupuserslength);
if(groupuserslength === 0 ){
controller.send('acknowledgeGroup',group,groupusersprocessed,groupuserslength);
}else{
    groupusers.forEach(function(groupuser){

      if(groupuser.get('role') === 'group'){
        groupuser.destroyRecord().then(function(){
          groupusersprocessed++;
          controller.send('acknowledgeGroup',group,groupusersprocessed,groupuserslength);
        });
      }else{

        var firstgroup = controller.store.findRecord('group',1);
        firstgroup.then(function(firstgroup){
          groupuser.set('group',firstgroup);
          groupuser.save().then(function(){
            groupusersprocessed++;
            controller.send('acknowledgeGroup',group,groupusersprocessed,groupuserslength);
          });
        });
      }
    });
}
  });

},

  saveGroup:function(group){
    var controller = this;
    controller.set('buttonDisabled',true);

    group.save().then(function(){
      controller.set('buttonDisabled',false);
       controller.notifications.addNotification({
        message:  'Saved' ,
        type: 'success',
        autoClear: true
      });

      group.get('users').forEach(function(user){
        
        user.save().then(function(){
      controller.transitionToRoute('dashboard.super.groups.index');
        });
      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
      });



    })



  }
}
});
