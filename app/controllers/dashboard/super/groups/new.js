import Ember from 'ember';

export default Ember.Controller.extend({

  isCreateGroupDisabled:Ember.computed('name' , 'password','password_confirmation','email',function(){
    if(Ember.isEmpty(this.get('name')) ||
    Ember.isEmpty(this.get('email')) ||
    Ember.isEmpty(this.get('password')) ||
    Ember.isEmpty(this.get('password_confirmation'))

  ){return 'disabled';}
  else{return '';}
}),



  checkPasswordSame:Ember.computed('password','password_confirmation',function(){
    if(this.get('password') === this.get('password_confirmation')){
      return false;
    }else{
      return true;
    }
  }),

  languages:['English','French'],

language:'English',
actions:{
  selectLanguage:function(language){
    var controller = this;
    controller.set('language',language);
  },

  createGroup:function(){
    var controller = this;

    var newGroup = controller.store.createRecord('group',{
      name: controller.get('name'),
    });

    newGroup.save().then(function(){


      var newUser = controller.store.createRecord('user',{
        username:controller.get('username'),
        email:controller.get('email'),
        password:controller.get('password'),
        language:controller.get('language'),
        role:'group',
      });



      newUser.save().then(function(){

controller.set('username','');
controller.set('name','');
controller.set('email','');
controller.set('password','');
controller.set('language','');
controller.set('group','');


        controller.notifications.addNotification({
          message:  'Group Created' ,
          type: 'success',
          autoClear: true
        });

          controller.transitionToRoute('dashboard.super.groups.index');
      });

    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something went wrong!' ,
        type: 'error',
        autoClear: true
      });
    });

  },



}
});
