import Ember from 'ember';


export default Ember.Controller.extend({


  checkPasswordSame:Ember.computed('password','password_confirmation',function(){



    if(this.get('password') === this.get('password_confirmation')  ){
      return false;
    }else{
      return true;
    }
  }),

  isCreateUserDisabled:Ember.computed('nameofinstitution', 'email',  'contactno' , 'password','password_confirmation',function(){
    var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;


    if(Ember.isEmpty(this.get('nameofinstitution')) ||
    Ember.isEmpty(this.get('email'))||
    Ember.isEmpty(this.get('contactno')) ||
    Ember.isEmpty(this.get('password')) ||
    Ember.isEmpty(this.get('password_confirmation'))||
    String(this.get('password')).length < 6 ||
    String(this.get('password_confirmation')).length < 6  ||
    !reg.test(this.get('email'))


  ){return true;}
  else{return false;}
}),

languages:['English','French'],
language:'English',




  actions:{

    selectLanguage:function(language){
      var controller = this;
      controller.set('language',language);
    },
    createUser:function(){


var controller = this;
controller.set('isCreateUserDisabled',true);
var firstgroup = controller.store.findRecord('group',1);
firstgroup.then(function(firstgroup){



var newUser = controller.store.createRecord('user',{
  nameofinstitution:controller.get('nameofinstitution'),
  email:controller.get('email'),
  location:controller.get('location'),
  address:controller.get('address'),
  contactno:controller.get('contactno'),
  walletbalance:controller.get('walletbalance'),
  paymentamount:controller.get('paymentamount'),
  password:controller.get('password'),
  installationdate: new Date(),
  nextpaymentdate: new Date(),
  group:firstgroup,
  noofpayments:1,
  role:'local',
  language:controller.get('language')
});



newUser.save().then(function(newUser){
  var i;
    for (i = 0; i < 7; i++) {
var newScheduleset = controller.store.createRecord('scheduleset',{
  description:'Description',
  schedulesetno: i+1,
  user:newUser
});

newScheduleset.save();
if(i===6){
  var j;
    for (j = 0; j < 2; j++) {
  var newExamScheduleset = controller.store.createRecord('examscheduleset',{
    user:newUser,
    description:'Description',
    examschedulesetno: j+1

  });

  newExamScheduleset.save();
  if(j===1){

    controller.set('nameofinstitution','');
    controller.set('email','');
    controller.set('location','');
    controller.set('address','');
    controller.set('contactno','');
    controller.set('walletbalance','');
    controller.set('paymentamount','');
    controller.set('password','');
    controller.set('password_confirmation','');

    controller.notifications.clearAll();
    controller.notifications.addNotification({
      message:  'User Created' ,
      type: 'success',
      autoClear: true
    });
    controller.set('buttonDisabled',false);






  }
}

}

  }

}).catch(function(error){

  controller.notifications.clearAll();
  controller.notifications.addNotification({
    message:  error.message ,
    type: 'error',
    autoClear: true
  });
  controller.set('buttonDisabled',false);

});
});
    }


  }

});
