import Ember from 'ember';

export default Ember.Controller.extend({


selectedUsers:null,
selectedGroup:null,

  userscolumns:[  {
    "template": "custom/checkbox",
    "useFilter": true,
    "mayBeHidden": true,
    "templateForSortCell": "custom/checkbox-all"
  },


  {
    "propertyName": "nameofinstitution",
    "title": "Name of Institution",
  },
  {
    "propertyName": "group.name",
    "title": "Group",
  },
  {
    "template": "custom/user-view",
    "title": "View",
  },

],


actions:{
  toggleAllSelection() {
    let selectedItems = get(this, '_selectedItems');
    let data = get(this, 'data');
    if(selectedItems.length === data.length) {
      get(this, '_selectedItems').clear();
    }
    else {
      set(this, '_selectedItems', A(data.slice()));
    }
    this.userInteractionObserver();
  },



  usersdisplayDataChanged:function(e){
    this.set('selectedUsers',e.selectedItems);
  },


groupSelected:function(group){
   this.set('selectedGroup',group);

},
  changeGroup:function(){
    var controller = this;

    var selectedGroup = controller.get('selectedGroup');
    if(Ember.isEmptyselectedGroup){
      console.log("empty")

    }else {
      console.log("notempty")
    }

    var selectedUsers = controller.get('selectedUsers');
    if(selectedUsers.length > 0){
      var sl = 0;
      var selectedUsersLength = selectedUsers.length;

      selectedUsers.forEach(function(selectedUser){
        selectedUser.set('group',selectedGroup);
        selectedUser.save().then(function(){
          sl++;
          if(sl === selectedUsersLength){
            controller.notifications.addNotification({
              message:  'Group Changed Successfuly' ,
              type: 'success',
              autoClear: true
            });
          }
        })
      });




    }
    else{
      controller.notifications.addNotification({
        message:  'Please Select any Users' ,
        type: 'error',
        autoClear: true
      });
    }
  }
}


});
