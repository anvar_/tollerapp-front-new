import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),


  actions:{
    impersonateUser:function(user){

      var controller = this;
      // var currentuserid =  this.get('session.data.authenticated.user_id');

      // controller.get('session').set('data.admin_id', currentuserid);
      controller.get('session').set('data.user_id', user.get('id'));
      controller.get('session').set('data.role', user.get('role'));
      controller.get('session').set('data.email', user.get('email'));
      controller.get('session').set('data.impersonated',true);

      controller.transitionToRoute('dashboard');
      window.location.reload(true);

    },


  }
});
