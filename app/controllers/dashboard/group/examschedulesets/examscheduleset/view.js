import Ember from 'ember';

export default Ember.Controller.extend({

sortProperties: ['time:asc'],
sortPropertiestimings: ['time:asc'],
  sortedExamassignations: Ember.computed.sort('examscheduleset.examassignations', 'sortProperties'),
  sortedExamtimings: Ember.computed.sort('examscheduleset.examtimings', 'sortPropertiestimings'),
  assignationDisabled:Ember.computed('sortedExamtimings',function(){
    var schedulesetTimingslength = this.get('sortedExamtimings.length');
    var schedulesetAssignations = this.get('examscheduleset').get('examassignations');
    if(schedulesetTimingslength===0){
      schedulesetAssignations.then(function(assignations){
        assignations.forEach(function(assignation){
          assignation.destroyRecord();
        });
      });
      return 'disabled';
    }else{
      return '';
    }
  }),



  sortedExamassignationsCount: Ember.computed.alias('examscheduleset.examassignations.length'),
  maxassignation:24,

  sortedExamtimingsCount: Ember.computed.alias('examscheduleset.examtimings.length'),

buttonDisabled:false,

  actions:{
    unAssignall:function(scheduleset){
      var controller = this;
      var allassignations = scheduleset.get('examassignations');
      allassignations.then(function(allassignations){

        var allassignationsprocessed = 0;
      var allassignationslength = scheduleset.get('examassignations.length');
      allassignations.forEach(function(assignation){
        assignation.destroyRecord().then(function(){
          allassignationsprocessed++;
          if(allassignationsprocessed === allassignationslength){
            controller.notifications.addNotification({
                message:  'All assignations deleted !' ,
                type: 'success',
                autoClear: true
              });
          }
        }).catch(function(){
            controller.notifications.addNotification({
                message:  'Something Went Wrong !' ,
                type: 'error',
                autoClear: true
              });
              controller.set('buttonDisabled',false);
        });
      });
      });

    },

    editDescription:function(examscheduleset){
      examscheduleset.set('editdescription',true);
    },


    saveDescription:function(examscheduleset){
      var controller = this;

      controller.set("buttonDisabled",true);
      examscheduleset.save().then(function(){
      examscheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      examscheduleset.set('editdescription',false);
      controller.set("buttonDisabled",false);
    });
  },

    doSomethingWithSelectedValue:function(examassignation , value){
      examassignation.set('time',value);
      examassignation.save();
    },


    deleteAssignation:function(examassignation){
      examassignation.destroyRecord();
    },



    deleteAllTimings:function(scheduleset){
      var controller = this;
      controller.set('buttonDisabled',true);

      var schedulesetTimings = scheduleset.get('examtimings');
      var schedulesetTimingsprocessed = 0;
      var schedulesetTimingslength = scheduleset.get('examtimings.length');

      schedulesetTimings.forEach(function(schedulesetTiming){
        schedulesetTiming.destroyRecord().then(function(){
          schedulesetTimingsprocessed++;
          if(schedulesetTimingsprocessed ===  schedulesetTimingslength){
            controller.notifications.addNotification({
              message:  'Timings deleted !' ,
              type: 'success',
              autoClear: true
            });
            controller.set('buttonDisabled',false);
          }

        }).catch(function(){
          controller.notifications.addNotification({
            message:  'Something Went Wrong !' ,
            type: 'error',
            autoClear: true
          });
          controller.set('buttonDisabled',false);
        });

      });
    },


    deleteTiming:function(timing){
    timing.destroyRecord();
    },

    addTiming:function(examscheduleset){
      var controller = this;
      controller.set('buttonDisabled',true);
      var schedulesettimngslength =  examscheduleset.get('examtimings.length');
      var newtime ;
      var defaultaudio = controller.store.peekRecord('audio',1);

    if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
      newtime  =  new Date('2017-05-05 08:00:00');
      var newTiming = controller.store.createRecord('examtiming',{
        examscheduleset:examscheduleset,
        time:newtime,
        audio:defaultaudio
      });

      newTiming.save().then(function(){

        controller.set('buttonDisabled',false);

      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });
    }else{
      var lastTime = controller.get('sortedExamtimings').get('lastObject').get('time');
      newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
      var schedulesetTimings = controller.get('sortedExamtimings');
     var schedulesetTimingsLength = controller.get('sortedExamtimings.length');
     var schedulesetTimingsprocessed = 0;
schedulesetTimings.forEach(function(timing) {


  var equalcounter = 0;

     schedulesetTimings.forEach(function(schedulesettiming) {
       if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
         equalcounter++;
        }
     });
     if(equalcounter < 2 ){// if same timing dont appear more than once.
if(timing.get('editTiming') === true){
       timing.set('editTiming',false);
       timing.save().then(function(){
         controller.set('buttonDisabled',false);
       }).catch(function(){
         controller.set('buttonDisabled',false);
         controller.notifications.addNotification({
           message:  'Something Went Wrong!' ,
           type: 'error',
           autoClear: true
         });
       });
}

     }else{
       controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
         message:  'Duplicate Times!' ,
         type: 'error',
         autoClear: true
       });
     }
     schedulesetTimingsprocessed++;

     if(schedulesetTimingsprocessed === schedulesetTimingsLength){
       var newTiming = controller.store.createRecord('examtiming',{
         examscheduleset:examscheduleset,
         time:newtime,
         audio:defaultaudio
       });

       newTiming.save().then(function(){

         controller.set('buttonDisabled',false);

       }).catch(function(){
         controller.notifications.addNotification({
           message:  'Something Went Wrong !' ,
           type: 'error',
           autoClear: true
         });
         controller.set('buttonDisabled',false);
       });
     }


});



    }


    },





    editTiming:function(timing){
 timing.set('editTiming',true);
    },

    saveAndAddAnother:function(timing ){

      var controller = this;
      var examscheduleset= controller.get('examscheduleset');
      controller.set('buttonDisabled',true);

      var examschedulesetTimings = controller.get('sortedExamtimings');
      var equalcounter = 0;


      examschedulesetTimings.forEach(function(schedulesettiming) {
        if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
          equalcounter++;
        }
      });


      if(equalcounter < 2 ){

        timing.set('editTiming',false);
        timing.save().then(function(){
          controller.set('buttonDisabled',false);


    ////
    controller.set('buttonDisabled',true);
    var schedulesettimngslength =  examscheduleset.get('examtimings.length');
    var newtime ;
    if(schedulesettimngslength === 0 ){  // if no timings new time is 8 am
    newtime  =  new Date('2017-05-05 08:00:00');
    }else{
    var lastTime = controller.get('sortedExamtimings').get('lastObject').get('time');
    newtime  = new Date(lastTime.getTime() + 40*60000);  // if timngs , add 40 mins to last time.
    }
    var defaultaudio = controller.store.peekRecord('audio',1);
    var newTiming = controller.store.createRecord('examtiming',{
      examscheduleset:examscheduleset,
      time:newtime,
      audio:defaultaudio
    });

    newTiming.save().then(function(){

      controller.set('buttonDisabled',false);

    }).catch(function(){
      controller.notifications.addNotification({
        message:  'Something Went Wrong !' ,
        type: 'error',
        autoClear: true
      });
      controller.set('buttonDisabled',false);
    });
    ////




        }).catch(function(){
          controller.set('buttonDisabled',false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong!' ,
            type: 'error',
            autoClear: true
          });
        });


      }else{
        controller.notifications.addNotification({
          message:  'Duplicate Times!' ,
          type: 'error',
          autoClear: true
        });
      }
    },


    saveTiming:function(timing){
      var controller = this;
      controller.set('buttonDisabled',true);

      var examschedulesetTimings = controller.get('sortedExamtimings');
      var equalcounter = 0;


      examschedulesetTimings.forEach(function(schedulesettiming) {
        if(schedulesettiming.get('time').getTime() === timing.get('time').getTime() ){
          equalcounter++;
        }
      });


      if(equalcounter < 2 ){

        timing.set('editTiming',false);
        timing.save().then(function(){
          controller.set('buttonDisabled',false);
        }).catch(function(){
          controller.set('buttonDisabled',false);
          controller.notifications.addNotification({
            message:  'Something Went Wrong!' ,
            type: 'error',
            autoClear: true
          });
        });


      }else{
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Duplicate Times!' ,
          type: 'error',
          autoClear: true
        });
      }



    },

    addAssignation:function(examscheduleset){
      var controller = this;
      if(controller.get('sortedExamtimings.length')!==0){
      controller.set('buttonDisabled',true);



      var newAssignation = controller.store.createRecord('examassignation',{
        examscheduleset:examscheduleset,
        time: new Date(),
      });
      newAssignation.save().then(function(){
        controller.set('buttonDisabled',false);
      }).catch(function(){
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
        controller.set('buttonDisabled',false);
      });
    }else
    {
      controller.notifications.addNotification({
        message:  'Add timings before assigning !' ,
        type: 'error',
        autoClear: true
      });
    }
    },


  }
});
