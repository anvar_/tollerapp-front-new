import Ember from 'ember';
import ENV from '../../../../config/environment';


const {A, on,get , getOwner, set, Logger} = Ember;
export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  ajax: Ember.inject.service(),

  selectedMemberUsers:null,




  memberuserscolumns:[  {
    "template": "custom/checkbox",
    "useFilter": true,
    "mayBeHidden": true,
    "templateForSortCell": "custom/checkbox-all"
  },
  {
    "propertyName": "id",
    "title": "Scheduleset ID",
  },
  {
    "propertyName": "nameofinstitution",
    "title": "Name",
  },

],


actions:{
  toggleAllSelection() {
    let selectedItems = get(this, '_selectedItems');
    let data = get(this, 'data');
    if(selectedItems.length === data.length) {
      get(this, '_selectedItems').clear();
    }
    else {
      set(this, '_selectedItems', A(data.slice()));
    }
    this.userInteractionObserver();
  },



  memberusersdisplayDataChanged:function(e){
    this.set('selectedMemberUsers',e.selectedItems);
  },

processAssignations:function(ap,al,sp,sl,mp,ml){
  var controller = this;
  if(ap===al){
    sp++;
    // console.log("t1-a1-spsl");
    // console.log(sp,sl);
    controller.send('processSchedulesets',sp,sl,mp,ml);


  }
},
processTimings:function(tp,tl,ap,al,sp,sl,mp,ml,selectedscheduleset,newScheduleset){
  var controller = this;
  if(tp===tl){

    if(al===0){
      sp++;

      controller.send('processSchedulesets',sp,sl,mp,ml);

    }else{
       selectedscheduleset.get('assignations').forEach(function(assignation){//could be empty
        var newAssignation =  controller.store.createRecord('assignation',{
          time:assignation.get('time'),
          day:assignation.get('day'),
          scheduleset:newScheduleset
        });
        newAssignation.save().then(function(){
          ap++;

          controller.send('processAssignations',ap,al,sp,sl,mp,ml);

        });

      });
    }

  }
},
processSchedulesets:function(sp,sl,mp,ml){
  var controller = this;
  if(sp===sl){
    mp++;
controller.send('processMembers',mp,ml);
  }
},
processMembers:function(mp,ml){
  var controller = this;
  console.log("mpml", mp,ml);
  if(mp === ml){
    console.log("mpml inside");
    controller.set('buttonDisabled',false);
    console.log("everything processed");
    controller.notifications.clearAll();
    controller.notifications.addNotification({
      message: 'Schedulesets Sent' ,
      type: 'success',
      autoClear: true
    });
  }
},
  //this model is to sendselectedschdeules to selected group users
  sendSelectedSchedulesets:function(){
    var controller = this;
    controller.set('modal1',false);

    controller.notifications.addNotification({
      message:  'Sending.... Please Wait.' ,
      type: 'info',
    });
    controller.set('buttonDisabled',true);
    var selectedMemberUsers = controller.get('selectedMemberUsers');
    var selectedSchedulesets  =  controller.get('schedulesets');

    if(selectedMemberUsers.length > 0){//check if users are selected.
      var mp=0;
      var ml = selectedMemberUsers.get('length');
      selectedMemberUsers.forEach(function(memberuser){

        memberuser.get('schedulesets').then(function(schedulesets){

          var usp = 0;
          var usl = schedulesets.get('length');


          schedulesets.forEach(function(sch) {
            sch.destroyRecord().then(function(){
              usp++;

              if(usp === usl){ // All member schedulesets are deleted

                var sp = 0;
                var sl = selectedSchedulesets.get('length');

                selectedSchedulesets.forEach(function(selectedscheduleset){
                  var newScheduleset = controller.store.createRecord('scheduleset',{
                    user:memberuser,
                    description:selectedscheduleset.get('description'),
                  });

                  newScheduleset.save().then(function(newScheduleset){

                    var timings = selectedscheduleset.get('timings');
                    var al =  selectedscheduleset.get('assignations.length');
                    // console.log("al:"+al);
                    var ap =0;
                    timings.then(function(timings){
                      var tp=0;
                      var tl = timings.get('length');
                      if(tl===0){
                        sp++;
                        // console.log("to-spsl")
                        // console.log(sp,sl);
                        controller.send('processSchedulesets',sp,sl,mp,ml);

                      }else{//if tl>0
                        timings.forEach(function(timing){//could be empty
                          var time =  timing.get('time');
                          var audio = timing.get('audio');
                          var newTiming =   controller.store.createRecord('timing',{
                            time:time,
                            audio:audio,
                            scheduleset:newScheduleset
                          });
                          newTiming.save().then(function(){
                            tp++;
                            controller.send('processTimings',tp,tl,ap,al,sp,sl,mp,ml,selectedscheduleset,newScheduleset);

                          });

                        });

                      }
                    });

                  });
                });
              }
            });
          });
        });
      });

    }else{
      controller.notifications.addNotification({
        message:  'Please Select any Users' ,
        type: 'error',
        autoClear: true
      });

    }



  },


  //this model is to sendselectedschdeules to selected group users
  sendSelectedExamSchedulesets:function(){
    var controller = this;

    var selectedMemberUsers = this.get('selectedMemberUsers');

    var selectedExamSchedulesets  =  this.get('selectedExamSchedulesets');


    if(selectedExamSchedulesets.length > 0){

      if(selectedMemberUsers.length > 0){
        selectedMemberUsers.forEach(function(memberuser){

          memberuser.get('examschedulesets').then(function(scheduleset){
            scheduleset.forEach(function(sch) {
              Ember.run.once(this, function() {
                sch.deleteRecord();
                sch.save();
              });
            }, this);

            selectedExamSchedulesets.forEach(function(selectedExamScheduleset){
              let newScheduleset = controller.store.createRecord('examscheduleset',{
                user:memberuser,
                description:selectedExamScheduleset.get('description'),
              });

              newScheduleset.save().then(function(newScheduleset){


                selectedExamScheduleset.get('examtimings').forEach(function(timing){
                  var newTiming =   controller.store.createRecord('examtiming',{
                    time:timing.get('time'),
                    audio:timing.get('audio'),
                    examscheduleset:newScheduleset
                  });
                  newTiming.save();
                });


                selectedExamScheduleset.get('examassignations').forEach(function(assignation){
                  var newAssignation =  controller.store.createRecord('examassignation',{
                    time:assignation.get('time'),
                    day:assignation.get('day'),
                    examscheduleset:newScheduleset
                  });
                  newAssignation.save();
                });

              });

            });

          });
        });


      }else{
        controller.notifications.addNotification({
          message:  'Please Select any Users' ,
          type: 'error',
          autoClear: true
        });

      }


    }
    else{
      controller.notifications.addNotification({
        message:  'Please Select any Schedulesets' ,
        type: 'error',
        autoClear: true
      });
      controller.set('modal2',false);
    }

  }


}
});
