import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),

  buttonDisabled:false,
  checkPasswordSame:Ember.computed('password','password_confirmation',function(){
    if(this.get('password') === this.get('password_confirmation')){
      return false;
    }else{
      return true;
    }
  }),
  actions:{



    saveUser:function(user){
      var controller = this;
      controller.set('buttonDisabled',true);

      user.set('password',controller.get('password'));
      user.set('password_confirmation',controller.get('password_confirmation'));
      user.save().then(function(){
        controller.set('buttonDisabled',false);
      }).catch(function(){
        controller.set('buttonDisabled',false);
        controller.notifications.addNotification({
          message:  'Something Went Wrong !' ,
          type: 'error',
          autoClear: true
        });
      });
    },
    impersonateUser:function(user){

      var controller = this;

      controller.get('session').set('data.user_id', user.get('id'));
      controller.get('session').set('data.role', user.get('role'));
      controller.get('session').set('data.email', user.get('email'));
      controller.get('session').set('data.impersonated',true);

      controller.transitionToRoute('dashboard');
      window.location.reload(true);

    },
  }
});
