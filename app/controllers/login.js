import Ember from 'ember';
export default Ember.Controller.extend(Ember.Evented,{

session: Ember.inject.service('session'),

  isLoginButtonDisabled: Ember.computed('username' , 'password', function() {

        if(Ember.isEmpty(this.get('username')) ||
        Ember.isEmpty(this.get('password'))

      ){return true;}
      else{return false;}
  }),

  actions: {
    authenticate(){
      var controller = this;


        this.get('session').authenticate('authenticator:devise', this.get('username'), this.get('password')).then(function(){
          var role = controller.get('session.data.role');
          if(role ==='local'){
          controller.transitionToRoute('dashboard.local.home');
          }
          else if(role ==='super'){
          controller.transitionToRoute('dashboard.super.index');
          }
          else if(role === 'group'){
            controller.transitionToRoute('dashboard.super.index');
          }
          else{
            controller.notifications.addNotification({
            message: 'Uncaught Error' ,
            type: 'error',
            autoClear: true
          });
          }

        }).catch(function(){

              controller.notifications.addNotification({
              message: 'Username or password is incorrect!' ,
              type: 'error',
              autoClear: true
            });

        });
    }
  },



});

// .catch(function(){
//
//   controller.notifications.addNotification({
//     message: 'Username or password is incorrect!' ,
//     type: 'error',
//     autoClear: true
//   });
// });
