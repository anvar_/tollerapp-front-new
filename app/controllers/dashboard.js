import Ember from 'ember';

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),

userEmail: Ember.computed('session', function() {
return (this.get('session.data.email'));
}),

userImpersonated: Ember.computed('session', function() {
return (this.get('session.data.impersonated'));
}),


actions:{
  deImpersonate:function(){
    var controller = this;

    controller.get('session').set('data.user_id', controller.get('session.data.authenticated.user_id'));
    controller.get('session').set('data.role', controller.get('session.data.authenticated.role'));
    controller.get('session').set('data.email', controller.get('session.data.authenticated.email'));
    controller.get('session').set('data.impersonated',false);

    controller.transitionToRoute('dashboard').then(function(){
    window.location.reload(true);
  });

  }
}

});
