import DS from 'ember-data';

import Ember from 'ember';

export default DS.Model.extend({
description: DS.attr('string'),
examschedulesetno: DS.attr('number'),
user:DS.belongsTo('user',{embeded: 'always',async:true}),
examassignations:DS.hasMany('examassignation',{embeded: 'always',async:true}),
examtimings:DS.hasMany('examtiming',{embeded: 'always',async:true}),
editdescription:DS.attr('boolean',{default:false}),





nextTiming: Ember.computed('examtimings.@each.time', 'datenow', function() {
   var timings = this.get('examtimings');
  var date = this.get('datenow');
  if(!date){
    date = new Date();
  }
  date.setFullYear(2000);
  date.setMonth(0);
  date.setDate(1);
  var sortedTimings = timings.sortBy('time');
  // console.log(sortedTimings);
   var sortedTimingsArray =  sortedTimings.filter(function(item){
  return date < item.get('time') ;
});

var firstObject = sortedTimingsArray.get('firstObject');

Ember.run.later(this, function() {
  sortedTimings.forEach(function(sortedTimingsobject){
    sortedTimingsobject.set('isNext',false);
  });
  if(firstObject){
     firstObject.set('isNext',true);
  }

}, 2000);

return sortedTimingsArray.get('firstObject');
}),



previousTiming: Ember.computed('examtimings.@each.time','datenow', function() {
var timings = this.get('examtimings');
var date = this.get('datenow');
if(!date){
  date = new Date();
}
date.setFullYear(2000);
date.setMonth(0);
date.setDate(1);
var sortedTimings = timings.sortBy('time');
 var sortedTimingsArray =  sortedTimings.filter(function(item){
return date > item.get('time') ;
});
var firstObject = sortedTimingsArray.get('lastObject');


Ember.run.later(this, function() {
  if(firstObject){
    sortedTimings.forEach(function(sortedTimingsobject){
      sortedTimingsobject.set('isPrevious',false);
    });

    if(sortedTimings.get('length') > sortedTimingsArray.get('length') ){
    firstObject.set('isPrevious',true);
    }
  }

}, 2000);




return sortedTimingsArray.get('lastObject');
}),





assigned: function() {
  var assignationslength = this.get('examassignations.length');
  // console.log('assigned');
  // console.log(assignationslength);
  if(assignationslength === 0){
    return false;
  }else{
    return true;
  }
}.property('examassignations'),

haveTimings: function() {
  var timingslength = this.get('examtimings.length');
  // console.log('timigns');
  // console.log(timingslength);
  if(timingslength === 0){
    return false;
  }else{
    return true;
  }
}.property('examtimings'),


});
