


import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  converttotimezone: Ember.inject.service('converttotimezone'),

  schedulesetno: DS.attr('number'),
  description: DS.attr('string'),
  timings:DS.hasMany('timing',{embeded: 'always',async:true}),
  assignations:DS.hasMany('assignation',{embeded: 'always',async:true}),
  user:DS.belongsTo('user',{embeded: 'always',async:true}),
  editdescription:DS.attr('boolean',{default:false}),
  datenow:DS.attr('date'),


  nextTiming: Ember.computed('timings.@each.time', 'datenow', function() {
    var timings = this.get('timings');
    var date = this.get('datenow');

    if(!date){
      date = this.get('converttotimezone').converttotimezone();
    }
    date.setFullYear(2000);
    date.setMonth(0);
    date.setDate(1);
    var sortedTimings = timings.sortBy('time');
     var sortedTimingsArray =  sortedTimings.filter(function(item){
    return date < item.get('time') ;
  });

  var firstObject = sortedTimingsArray.get('firstObject');

  Ember.run.later(this, function() {
    sortedTimings.forEach(function(sortedTimingsobject){
      sortedTimingsobject.set('isNext',false);
    });
    if(firstObject){
       firstObject.set('isNext',true);
    }

  }, 2000);

return sortedTimingsArray.get('firstObject');
}),



previousTiming: Ember.computed('timings.@each.time','datenow', function() {
  var timings = this.get('timings');
  var date = this.get('datenow');
  if(!date){
    // date = new Date();
    date = this.get('converttotimezone').converttotimezone();
  }
  date.setFullYear(2000);
  date.setMonth(0);
  date.setDate(1);
  var sortedTimings = timings.sortBy('time');
   var sortedTimingsArray =  sortedTimings.filter(function(item){
  return date > item.get('time') ;
});
var firstObject = sortedTimingsArray.get('lastObject');


  Ember.run.later(this, function() {
    if(firstObject){
      sortedTimings.forEach(function(sortedTimingsobject){
        sortedTimingsobject.set('isPrevious',false);
      });

      if(sortedTimings.get('length') > sortedTimingsArray.get('length') ){
      firstObject.set('isPrevious',true);
      }
    }

  }, 2000);




return sortedTimingsArray.get('lastObject');
}),


  assigned: function() {
    var assignationslength = this.get('assignations.length');
    if(assignationslength === 0){
      return false;
    }else{
      return true;
    }
  }.property('assignations'),

  haveTimings: function() {
    var timingslength = this.get('timings.length');

    if(timingslength === 0){
      return false;
    }else{
      return true;
    }
  }.property('timings'),







    mondayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');

      return assignations.any(function(assignation){
        if(assignation.get('day')==='monday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),



    tuesdayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');

      return assignations.any(function(assignation){
        if(assignation.get('day')==='tuesday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),


    wednesdayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');
      return assignations.any(function(assignation){
        if(assignation.get('day')==='wednesday'){
          return true;
        }else{
          return false;
        }
      });

    }.property('assignations.@each.day'),


    thursdayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');
      return assignations.any(function(assignation){
        if(assignation.get('day')==='thursday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),


    fridayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');
      return assignations.any(function(assignation){
        if(assignation.get('day')==='friday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),



    saturdayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');
      return assignations.any(function(assignation){
        if(assignation.get('day')==='saturday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),


    sundayAssigned: function() {
      // var scheduleset = this.get('scheduleset');
      var assignations = this.get('assignations');
      return assignations.any(function(assignation){
        if(assignation.get('day')==='sunday'){
          return true;
        }else{
          return false;
        }
      });
    }.property('assignations.@each.day'),






});
