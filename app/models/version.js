import DS from 'ember-data';

export default DS.Model.extend({
itemtype:DS.attr('string'),
itemid:DS.attr('number'),
event:DS.attr('string'),
whodunnit:DS.attr('number'),
whodunnitemail:DS.attr('string')


});
