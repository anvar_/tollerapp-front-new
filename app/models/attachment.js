import DS from 'ember-data';

export default DS.Model.extend({
  url: DS.attr('string'),
  fullurl: DS.attr('string'),
  filename: DS.attr('string'),
  reply: DS.belongsTo('reply' , {embeded: 'always',async:true}),
  ticket: DS.belongsTo('ticket' , {embeded: 'always',async:true}),
});
