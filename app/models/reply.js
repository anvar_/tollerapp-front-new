import DS from 'ember-data';

export default DS.Model.extend({
  body: DS.attr('string'),
  user: DS.belongsTo('user' , {async:true }),
  ticket: DS.belongsTo('ticket' , {async:true }),
  editreply:DS.attr('boolean',{default:false}),
  attachments:DS.hasMany('attachment',{embeded: 'always',async:true}),
  createdat:DS.attr('date')





});
