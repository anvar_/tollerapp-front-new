import DS from 'ember-data';

export default DS.Model.extend({
  time:DS.attr('date'),
  audio:DS.belongsTo('audio',{embeded: 'always',async:true}),
  examscheduleset:DS.belongsTo('examscheduleset',{embeded: 'always',async:true}),
  editTiming:DS.attr('boolean',{default:false})
});
