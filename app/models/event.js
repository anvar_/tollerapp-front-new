import DS from 'ember-data';

export default DS.Model.extend({
title: DS.attr('string'),
startsat: DS.attr('date'),
endsat: DS.attr('date'),
});
