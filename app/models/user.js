import DS from 'ember-data';

export default DS.Model.extend({
  username:DS.attr('string'),
  email:DS.attr('string'),
  address:DS.attr('string'),
  state:DS.attr('string'),
  country:DS.attr('string'),
  role:DS.attr('string'),
  name:DS.attr('string'),
  dyndns:DS.attr('string'),
  uniqueidentifier:DS.attr('string'),
  authenticationtoken:DS.attr('string'),
  language:DS.attr('string',{default:'English'}),
  internetport:DS.attr('string'),
  ethernetport:DS.attr('string'),
  nameofinstitution:DS.attr('string'),
  contactno:DS.attr('string'),
  location:DS.attr('string'),
  password:DS.attr('string'),
  password_confirmation:DS.attr('string'),
  installationdate:DS.attr('date'),
  noofpayments:DS.attr('number'),
  nextpaymentdate:DS.attr('date'),
  paymentamount:DS.attr('number'),
  walletbalance:DS.attr('number'),
  fcm: DS.belongsTo('fcm' ,{embedded: 'always', async:true}),
  audios: DS.hasMany('audio' ,{embedded: 'always', async:true}),
  schedulesets: DS.hasMany('scheduleset' ,{embedded: 'always', async:true}),
  examschedulesets: DS.hasMany('examscheduleset' ,{embedded: 'always', async:true}),
  tickets: DS.hasMany('ticket' ,{embedded: 'always', async:true}),
  payments: DS.hasMany('payment' ,{embedded: 'always', async:true}),
  walletpayments:DS.hasMany('walletpayment' ,{embedded: 'always', async:true}),
  group:DS.belongsTo('group',{embeded: 'always',async:true}),
  isowner:DS.attr('boolean'),
  timezone:DS.attr('string'),
  checkPasswordSame:Ember.computed('password','password_confirmation',function(){
    if(this.get('password') === this.get('password_confirmation')){
      return false;
    }else{
      return true;
    }
  }),


});
