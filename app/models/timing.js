import DS from 'ember-data';

export default DS.Model.extend({
  
  time: DS.attr('date'),
  title: DS.attr('string') ,
  audio:DS.belongsTo('audio',{embeded: 'always',async:true},{default:1}),
  scheduleset:DS.belongsTo('scheduleset',{embeded: 'always',async:true}),
  zone1: DS.attr('boolean') ,
  zone2: DS.attr('boolean'),
  zone3: DS.attr('boolean') ,
  zone4: DS.attr('boolean'),
  // zone4: DS.attr('boolean',{defaultValue:true}),
  
  editTiming:DS.attr('boolean',{default:false}),
  isPrevious:DS.attr('boolean' ,{default:false}),
  isNext:DS.attr('boolean',{default:false}),




});
