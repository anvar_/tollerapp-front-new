import DS from 'ember-data';

export default DS.Model.extend({




  token: DS.attr('string'),
  webaudiostatus: DS.attr('string'),
  webmutestatus: DS.attr('string'),
  deviceonlinestatus: DS.attr('string'),
  deviceaudiostatus: DS.attr('string'),
  devicemutestatus: DS.attr('string'),
  pendingonlinestatus: DS.attr('string'),
  pendingsyncstatus: DS.attr('string'),
  pendingaudiostatus: DS.attr('string'),
  pendingmutestatus: DS.attr('string'),






user: DS.hasMany('user' , {async:true }),
});
