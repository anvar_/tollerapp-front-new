import DS from 'ember-data';


export default DS.Model.extend({
title: DS.attr('string'),
body: DS.attr('string'),
status: DS.attr('string'),
user:DS.belongsTo('user',{embeded: 'always',async:true}),
replies:DS.hasMany('reply',{embeded: 'always',async:true}),
attachments:DS.hasMany('attachment',{embeded: 'always',async:true}),
editdescription:DS.attr('boolean',{default:false}),
editbody:DS.attr('boolean',{default:false}),
createdat:DS.attr('date')
});
