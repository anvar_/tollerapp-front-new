import Ember from 'ember';
const {
  getOwner
} = Ember;
export default Ember.Component.extend({
hours:['Select Hour','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23'],
realhour: Ember.computed('timing', function() {
        var hour = this.get('timing').get('time').getHours();
       if (hour < 10)
   {
       return ("0" + hour.toString());
   }
   else
   {
       return hour.toString();
   }

    }),

  actions:{
    selectHour:function(hour){
      // console.log(getOwner(this).lookup('controller:application'));
      // console.log(hour);
      var  timing = this.get('timing');
      var time = timing.get('time');
      time.setHours(hour);


      // console.log(time);
      timing.set('time',time);
      // timing.save().then(function(){
      // });
    }
  }
});
