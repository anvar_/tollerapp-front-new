<?php
/**
 * Template Name: Blank Page
 *
 * The template for displaying a full width, unstyled page
 *
 * @package Layers
 * @since Layers 1.0.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<?php wp_head(); ?>
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400" rel="stylesheet">
	<style>

	body
	{	font-family: 'Ubuntu', sans-serif !important;

	}
#wrapper-site, .wrapper-site{
		background:url('/wp-content/themes/layerswp/assets/images/asanoha-400px.png')!important;
}
	.header-site, .header-site.header-sticky {
    background-color: #4A5899;
    color: #fff;
	}
	.sitename a
	{ color:#fff;}
	.registerbox{border: 0px solid #4a5899;padding: 15px;border-radius: 10px;margin: 10px;background:#fff;box-shadow: 0 10px 16px 0 rgba(74,88,153,00.5),0 6px 20px 0 rgba(74,88,153,.5)  !important;}	
input[type=button], input[type=submit], button {
    display: inline-block;
    width: auto;
    padding: 5px 10px;
    vertical-align: middle;
    background-color: #fff;
    border: 2px solid #4a5899;
    text-align: center;
    color: #4a5899;
    font-size: 1.7rem;
    font-weight: 700;
    font-style: normal;
    cursor: pointer;
    -webkit-appearance: none;
    -moz-appearance: none;
    transition: all 0.2s ease-in-out;
    border-radius: 4px;
    margin: 10px 0px 0px -20px;
    font-family: 'Ubuntu', sans-serif !important;
}
.nav-horizontal li a {
    display: block;
    color: #fff;
    text-align: center;
    font-weight: 700;
}


*
{text-shadow:none !important;
}

.created-using-layers
{
display:none !important;
}
	</style>
</head>
<body <?php body_class(); ?>>
	<?php get_sidebar( 'off-canvas'); ?>
	<?php do_action( 'layers_before_site_wrapper' ); ?>
	<div <?php layer_site_wrapper_class(); ?>>

		<?php do_action( 'layers_before_header' ); ?>

		<?php /*get_template_part( 'partials/header' , 'secondary' );*/ ?>

		<?php if ( 'header-sidebar' == layers_get_theme_mod( 'header-menu-layout' ) ) { ?>
			<div class="header-side-wrapper"><!-- header side wrapper -->

		<?php } ?>

		<section <?php layers_header_class(); ?>>
			<?php do_action( 'layers_before_header_inner' ); ?>
            <div class="<?php if( 'layout-fullwidth' != layers_get_theme_mod( 'header-width' ) ) echo 'container'; ?> header-block">
				<?php if( 'header-logo-center' == layers_get_theme_mod( 'header-menu-layout' ) ) {
					get_template_part( 'partials/header' , 'centered' );
				} else {
					get_template_part( 'partials/header' , 'standard' );
				} // if centered header ?>
			</div>
			<?php do_action( 'layers_after_header_inner' ); ?>
		</section>

		<?php do_action( 'layers_after_header' ); ?>

		<section id="wrapper-content" <?php layers_wrapper_class( 'wrapper_content', 'wrapper-content' ); ?>>





<div id="post-<?php the_ID(); ?>" <?php post_class( 'container content-main clearfix' ); ?>>
    <?php if( have_posts() ) : ?>
        <?php while( have_posts() ) : the_post(); ?>
            <div class="grid">
		 <div class="column span-3"></div>	
                <div class="column span-6 registerbox">
                    <?php get_template_part( 'partials/content', 'single' ); ?>
                </div>
		 <div class="column span-3"></div>	
            </div>
        <?php endwhile; // while has_post(); ?>
    <?php endif; // if has_post() ?>
</div>


			<div id="back-to-top">
				<a href="#top"><?php _e( 'Back to top' , 'layerswp' ); ?></a>
			</div> <!-- back-to-top -->

			<?php if ( 'header-sidebar' == layers_get_theme_mod( 'header-menu-layout' ) ) { ?>
				<?php get_template_part( 'partials/footer' , 'standard' ); ?>
			<?php } ?>

		</section>


		<?php if ( 'header-sidebar' == layers_get_theme_mod( 'header-menu-layout' ) ) { ?>
			</div><!-- /header side wrapper -->
		<?php } else {
			get_template_part( 'partials/footer' , 'standard' );
		}?>

	</div><!-- END / MAIN SITE #wrapper -->
	<?php do_action( 'layers_after_site_wrapper' ); ?>
	<?php wp_footer(); ?>
</body>
</html>
